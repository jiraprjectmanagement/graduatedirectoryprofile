<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Habib_Graduate_Directory
 */

get_header(); ?>

	<main id="primary" class="single site-main">

		<div class="back-page-button">
			<div class="main-btn">
				<!-- <a onclick="history.back();" class="btn" style=""> -->
				<a href="javascript:void(0);" onclick="goBack()" class="btn" style="">
					<span class="btn-txt-one"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to list</span>
					<span class="btn-txt-two"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to list</span>
				</a>
			</div>
		</div>

		<?php while ( have_posts() ) : the_post(); ?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



	<div class="single-main-post-content">
		<div class="single-post-left">
			<div id="profile" class="profile-section single-profile">
			<div class="single-information">
					<div class="single-main-post-thumbnail">
						<?php 	if ( has_post_thumbnail() ) {
								the_post_thumbnail();
							} else { ?>
								<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/04/default-featured-image.jpg" alt="<?php the_title(); ?>" />
							<?php } ?>
						<?php //graduate_directory_post_thumbnail(); ?>	
					</div>
					<div class="single-information-content">
					<div class="video-link">
								<?php if( get_field('video_profile_link') ): ?>
									<i class="fa fa-play" aria-hidden="true"></i><a class="" href="<?php the_field('video_profile_link'); ?>" data-lity> Watch Video Profile </a>
								<?php endif; ?>
					</div>
					<header class="entry-header">
						<?php
							if ( is_singular() ) :
								the_title( '<h1 class="entry-title">', '</h1>' );
							else :
								the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
							endif;

							if ( 'post' === get_post_type() ) :
						?>
						<div class="entry-meta">
							<?php
								graduate_directory_posted_on();
								graduate_directory_posted_by();
							?>
						</div>
							<?php endif; ?>
					</header>
					<div class="program-name">
						<?php
							$categories = get_the_terms( $post->ID, 'program' );
							if (is_array($categories) || is_object($categories)) {
							foreach( $categories as $category ) {
									//echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
								if(!$category->parent == 0)  {
									// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
									// echo $category->name .'</a><br />'; }
									echo $category->name .'<br />'; }
							}
						 } ?>
					</div>
					<div class="class-name">
						<?php
							$categories = get_the_terms( $post->ID, 'class' );
							if (is_array($categories) || is_object($categories)) {
								foreach( $categories as $category ) {
									// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
									// echo $category->name . '</a><br />';
									echo $category->name . '<br />';
								}
							}
						?>
					</div>
					<div class="single-contect-info">
							<div class="email">
								<?php if( get_field('email') ): ?>
									<i class="fa fa-envelope" aria-hidden="true"></i><a class="" href="mailto:<?php the_field('email'); ?>"> <?php the_field('email'); ?></a>
								<?php endif; ?>
							</div>
							<div class="phone">
								<?php if( get_field('phone') ): ?>
									<i class="fa fa-phone" aria-hidden="true"></i><a class="" href="tel:<?php the_field('phone'); ?>"> <?php the_field('phone'); ?></a>
								<?php endif; ?>
							</div>
							<div class="linkedin_link">
								<?php if( get_field('linkedin_link') ): ?>
									<i class="fab fa-linkedin-in"></i><a class="" href="<?php the_field('linkedin_link'); ?>" data-lity><span> linkedin.com/</span><?php the_title( );//the_field('linkedin_link'); ?></a>
								<?php endif; ?>
							</div>
							<div class="portfolio_link">
								<?php if( get_field('portfolio_link') ): ?>
									<i class="fa fa-globe" aria-hidden="true"></i><a class="" href="<?php the_field('portfolio_link'); ?>" data-lity><span> Portfolio  Website</span></a>
								<?php endif; ?>
							</div>
					</div>
				</div>
			</div>

			
		</div>
	</div>





		<div class="single-post-right">
			<!-- <div class="entry-content">
				 <?php  // the_content( sprintf( wp_kses( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'graduate-directory' ),
						//	array(
						//		'span' => array(
						//		'class' => array(),
						//	), ) ),
						//wp_kses_post( get_the_title() ) ) );
						//wp_link_pages( array(
						//	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'graduate-directory' ),
						//	'after'  => '</div>', ) ); 	?>
			</div> -->
			<div class="aspiration_statement">
				<?php if( get_field('aspiration_statement') ): ?>
					<h2 class="main-heading">Aspiration Statement</h2>
					<p class=""><?php the_field('aspiration_statement'); ?></p>
				<?php endif; ?>
			</div>
		
			<div id="academic" class="profile-section single-academic">
				<div class="core_skills">
				<h2>Core Skills</h2>
					<?php 
						$rows = get_field('core_skills');
							if( $rows ) {
    							echo '<ul class="skill">';
    								foreach( $rows as $row ) {
        							// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
									echo '<li>';
									echo $row['skill'];
       								echo '</li>';
    							}
   						 	echo '</ul>';
						}
					?>
			</div>
			<div class="Academic Awards">
				<h2>Academic Awards / Achievements</h2>
				<?php 
						$rows = get_field('academic_awards');
							if( $rows ) {
    							echo '<ul class="">';
    								foreach( $rows as $row ) {
        							// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
									echo '<li>';
									echo $row['academic_record'];
       								echo '</li>';
    							}
   						 	echo '</ul>';
						}
					?>
			</div>
		</div>

		<div id="experience" class="profile-section single-experience">
			<div class="leadership-experiences">
			<h2 class="main-heading">Experience</h2>
				<h2 class="color-heading">Leadership /Meta-curricular</h2>
					<?php 
						$rows = get_field('leadership-experiences');
							if( $rows ) {
    							echo '<ul class="leadership">';
    								foreach( $rows as $row ) {
        							// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
									echo '<li>';
									echo $row['leadership'];
       								echo '</li>';
    							}
   						 	echo '</ul>';
						}
					?>
			</div>
			<div class="work-experience">
				<h2 class="color-heading">Internship / Volunteer Work</h2>
					<?php 
						$rows = get_field('work-experience');
							if( $rows ) {
    							echo '<ul class="experience">';
    								foreach( $rows as $row ) {
        							// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
									echo '<li>';
									echo $row['experience'];
       								echo '</li>';
    							}
   						 	echo '</ul>';
						}
					?>
			</div>
			<div class="publications-projects">
				<h2 class="main-heading">Publications or Creative Projects</h2>
					<?php 
						$rows = get_field('publications-projects');
							if( $rows ) {
    							echo '<ul class="publications">';
    								foreach( $rows as $row ) {
        							// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
									echo '<li>';
									echo $row['publications'];
       								echo '</li>';
    							}
   						 	echo '</ul>';
						}
					?>
			</div>
		</div>

		<div id="fyp" class="profile-section single-fyp">
			<div class="fyp_desc">
				<h2 class="main-heading">Final Year Project</h2>
				<h2 class="color-heading">Decription</h2>
				<?php if( get_field('fyp_desc') ): ?>
						<p><?php the_field('fyp_desc'); ?></p>
					<?php endif; ?>
			</div>
			<div class="project_pictures">
			<h2 class="color-heading">Project Pictures</h2>
				<?php 
					$rows = get_field('project_pictures');
						if( $rows ) {
    					echo '<ul class="slides">';
    						foreach( $rows as $row ) {
        						$image = $row['project_pic'];
        						echo '<li>';
							?> 
								<a href="<?php echo $image['url']; ?>" data-lity>
									<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
	   							</a>
	   							<!-- <p><?php //echo $image['caption']; ?></p> -->
	   						<?php
        						echo '</li>';
    						}
   						 echo '</ul>';
						}?>
			</div>
			<div class="project-video">
			<h2 class="color-heading">Project Video</h2>
				<?php if( get_field('project-video') ): ?>
						<!-- <p><?php the_field('project-video'); ?></p> -->
						<a class="" href="<?php the_field('project-video'); ?>" data-lity><i class="fa fa-link" aria-hidden="true"></i></a> <span>_______</span><span> video profile link will be place over here</span>
					<?php endif; ?>
			</div>
		</div>
</div>
		
	</div>

	<footer class="entry-footer">
		<?php //graduate_directory_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->

<?php
			//get_template_part( 'template-parts/content', get_post_type() );

			// the_post_navigation(
			// 	array(
			// 		'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'graduate-directory' ) . '</span> <span class="nav-title">%title</span>',
			// 		'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'graduate-directory' ) . '</span> <span class="nav-title">%title</span>',
			// 	)
			// );
			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;
		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
//get_sidebar();
get_footer();
