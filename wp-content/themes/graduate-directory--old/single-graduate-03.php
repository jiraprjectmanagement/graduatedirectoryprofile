<?php
   /**
    * The template for displaying all single posts
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
    *
    * @package Habib_Graduate_Directory
    */
   
   /*
     Template Name: single-graduate-03
     Template Post Type: post, page, graduate
    */
   
   get_header('03'); ?>
<main id="primary" class="single site-main aspiration-profiles">
   <?php while ( have_posts() ) : the_post(); ?>
   <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <section class="profil-details-wrapper">
         <div class="container">
            <div class="profile-back">
               <a href="javascript:void(0);" class="share-btn"> <img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/share-icon.svg" alt=""> Share Profile</a>
               <a href="javascript:void(0);" class="back-btn btn-default gardi-dflt" onclick="goBack()">Back to list</a>
            </div>
            <div class="share-overlay" style=""></div>
            <div class="share-popup"><span class="share-cloes-btn"><i class="fa fa-times" aria-hidden="true"></i></span><?php wcr_share_buttons(); ?></div>
            <div class="profile-details-inner">
               <div class="student-profile-details">
                        <div class="student-profile">
                           <span class="prof-bg">
                             <?php 	if ( has_post_thumbnail() ) {
                                the_post_thumbnail();
                                } else { ?>
                             <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/04/default-featured-image.jpg" alt="<?php the_title(); ?>" />
                             <?php } ?>
                             <?php //graduate_directory_post_thumbnail(); ?>	
                             <!-- <img src="<?php //bloginfo('template_url'); ?>/img/profile.jpg" alt="" class="user-img"> -->
                          </span>
                          <?php if( get_field('video_profile_link') ): ?>
                              <a href="<?php the_field('video_profile_link'); ?>" class="video-prof-icon gardi-dflt">
                              <img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/play-icon.svg" alt="">
                              Watch Video Profile
                           </a>
                           <?php endif; ?>
                         
                        </div>
                        <div class="user-detail-inner user-profile boxpad-tb mobile-top-profile">
                     
                           <div class="user-detail">
                              <div>
                                 <?php
                                    if ( is_singular() ) :
                                       the_title( '<h2 class="entry-title">', '</h2>' );
                                    else :
                                       the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                                    endif;
                                    
                                    if ( 'post' === get_post_type() ) :
                                    ?>
                                 <div class="entry-meta">
                                    <?php
                                       graduate_directory_posted_on();
                                       graduate_directory_posted_by();
                                       ?>
                                 </div>
                                 <?php endif; ?>
                              </div>
                              <p>
                                 <?php
                                    $categories = get_the_terms( $post->ID, 'class' );
                                    if (is_array($categories) || is_object($categories)) {
                                       foreach( $categories as $category ) {
                                          // echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
                                          // echo $category->name . '</a><br />';
                                          echo $category->name . '<br />';
                                       }
                                    }
                                    ?>
                              </p>
                              <p class="program-bg">
                                 <?php
                                    $categories = get_the_terms( $post->ID, 'program' );
                                    if (is_array($categories) || is_object($categories)) {
                                    foreach( $categories as $category ) {
                                          //echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
                                       if(!$category->parent == 0)  {
                                          // echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
                                          // echo $category->name .'</a><br />'; }
                                          echo $category->name .'<br />'; }
                                    }
                                    } ?>
                              </p>
                           </div>
                        </div>
                        
                        <ul class="profile-social br-b-res">
                                 <li>
                                    <?php if( get_field('email') ): ?>
                                    <a class="" href="mailto:<?php the_field('email'); ?>"> <img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/em.svg" alt=""> 
                                    <p> <?php the_field('email'); ?></p>
                                    </a>
                                    <?php endif; ?>
                                 </li>
                                 <li>
                                    <?php if( get_field('linkedin_link') ): ?>
                                    <a class="" href="tel:<?php the_field('linkedin_link'); ?>">  <img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/ln.svg" alt="">
                                    <p> <?php the_field('linkedin_title'); ?></p>
                                    </a>
                                    <?php endif; ?>
                                 </li>
                                 <li>
                                    <?php if( get_field('portfolio_link') ): ?>
                                    <a class="" href="tel:<?php the_field('portfolio_link'); ?>">  <img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/pw.svg" alt="">
                                        <p> <?php the_field('portfolio_title'); ?></p>
                                    </a>
                                    <?php endif; ?>
                                 </li>
                              </ul>
               </div>
               <div class="student-details">
                  <div class="tab-container">
                     <div class="user-detail-inner user-profile boxpad-tb desck-top-profile">
                     
                        <div class="user-detail">
                           <div>
                              <?php
                                 if ( is_singular() ) :
                                 	the_title( '<h2 class="entry-title">', '</h2>' );
                                 else :
                                 	the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                                 endif;
                                 
                                 if ( 'post' === get_post_type() ) :
                                 ?>
                              <div class="entry-meta">
                                 <?php
                                    graduate_directory_posted_on();
                                    graduate_directory_posted_by();
                                    ?>
                              </div>
                              <?php endif; ?>
                           </div>
                           <p>
                              <?php
                                 $categories = get_the_terms( $post->ID, 'class' );
                                 if (is_array($categories) || is_object($categories)) {
                                 	foreach( $categories as $category ) {
                                 		// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
                                 		// echo $category->name . '</a><br />';
                                 		echo $category->name . '<br />';
                                 	}
                                 }
                                 ?>
                           </p>
                           <p class="program-bg">
                              <?php
                                 $categories = get_the_terms( $post->ID, 'program' );
                                 if (is_array($categories) || is_object($categories)) {
                                 foreach( $categories as $category ) {
                                 		//echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
                                 	if(!$category->parent == 0)  {
                                 		// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
                                 		// echo $category->name .'</a><br />'; }
                                 		echo $category->name .'<br />'; }
                                 }
                                 } ?>
                           </p>
                        </div>
                     </div>
                     <div  class="tab tab-active" data-id="tab1">
                        <div class="tab-scroll">
                           <div class="user-details-wraper">
                              <div class="user-detail boxpad-tb animation-layer pad-r br-b-res">
                                 <?php if( get_field('aspiration_statement') ): ?>
                                    <h3>Aspiration Statement</h3>
                                    <p class=""><?php the_field('aspiration_statement'); ?></p>
                                 <?php endif; ?>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--end of tab one--> 
                     <div  class="tab " data-id="tab2">
                        <div class="tab-scroll">
                           <div class="user-details-wraper">
                              <div class="user-detail detail-box boxpad-tb animation-layer br-b-res">
                                 <?php if( get_field('core_skills') ): ?>
                                    <h3>Core Skills</h3>
                                    <?php 
                                       $rows = get_field('core_skills');
                                          if( $rows ) {
                                             echo '<ul class="skills-bullets">';
                                                foreach( $rows as $row ) {
                                                // echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
                                                echo '<li><img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/bullet-point.svg" alt="">';
                                                echo $row['skill'];
                                                   echo '</li>';
                                             }
                                             echo '</ul>';
                                       }
                                    ?>
                                 <?php endif; ?>
                                 <!-- <ul class="skills-bullets">
                                    <li> <img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Design Research/ Designing</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Strong Communication</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Photography</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Writing Skills in English and Urdu</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Problem Solving</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Film making / Videography/ Video Editing</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Proficient in Adobe Softwares</li>
                                 </ul> -->
                              </div>
                              <div class="user-detail detail-box animation-layer academic-award">
                              <?php if( get_field('academic_awards') ): ?>
                                    <h3>Academic Awards / Achievements</h3>
                                 <?php 
                                       $rows = get_field('academic_awards');
                                          if( $rows ) {
                                             echo '<ul class="skills-bullets">';
                                                foreach( $rows as $row ) {
                                                // echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
                                                echo '<li><img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/bullet-point.svg" alt=""> ';
                                                echo $row['academic_record'];
                                                   echo '</li>';
                                             }
                                             echo '</ul>';
                                       }
                                    ?>
                                 <?php endif; ?>
                                 <!-- <ul class="skills-bullets bullet-100">
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">HU TOPS 100% Scholarship</li>
                                 </ul> -->
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--end of tab two--> 
                     <div  class="tab " data-id="tab3">
                        <div class="tab-scroll">
                           <div class="user-details-wraper">
                              <div class="user-detail boxpad-tb detail-box animation-layer">
                                 <h3>Experiences</h3>
                                 <div class="experience-bullet">
                                    <!-- Leadership -->
                                    <?php if( get_field('leadership-experiences') ): ?>
                                    <?php endif; ?>
                                       <?php 
                                          $rows = get_field('leadership-experiences');
                                             if( $rows ) {
                                                echo '<ul class="skills-bullets">';
                                                echo '<h4>Leadership Experiences /Meta-curricular</h4>';
                                                
                                                   foreach( $rows as $row ) {
                                                   echo '<li><img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/bullet-point.svg" alt=""> ';
                                                   echo $row['leadership'];
                                                      echo '</li>';
                                                }
                                                echo '</ul>';
                                          }
                                       ?>
                                    <!-- Leadership -->
   
                                    <!-- Internship -->
                                     <?php if( get_field('work-experience') ): ?>
                                          <?php endif; ?>
                                             <?php 
                                                $rows = get_field('work-experience');
                                                   if( $rows ) {
                                                      echo '<ul class="skills-bullets">';
                                                      echo '<h4>Internship / Volunteer Work</h4>';
                                                         foreach( $rows as $row ) {
                                                         echo '<li><img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/bullet-point.svg" alt=""> ';
                                                         echo $row['experience'];
                                                            echo '</li>';
                                                      }
                                                      echo '</ul>';
                                                }
                                    ?>
                                    <!-- Internship -->
   
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--end of tab three--> 
                     <div  class="tab " data-id="tab4">
                        <div class="tab-scroll">
                           <div class="user-details-wraper">
                              <div class="user-detail boxpad-tb pad-r animation-layer br-t-res">
                                 <div class="project-content">
                                    <h3>Final Year Project</h3>
                                    <?php if( get_field('fyp_desc') ): ?>
                                          <h4 class="project_title"><?php the_field('project_title'); ?></h4>
                                          <p><?php the_field('fyp_desc'); ?></p>
                                    <?php endif; ?>
                                    
                                 </div>
                                 <div class="project-content">
                                    <?php if( get_field('project_pictures') ): ?>
                                 <h4 class="color-heading">Project Pictures</h4>
                                 <?php endif; ?>
                                 <?php 
                                       $rows = get_field('project_pictures');
                                          if( $rows ) {
                                          echo '<div class="project-pic owl-carousel">';
                                             foreach( $rows as $row ) {
                                                $image = $row['project_pic'];
                                                echo '<div class="proj-box">';
                                             ?> 
                                                <a href="<?php echo $image['url']; ?>" data-lity>
                                                   <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                   </a>
                                                   <!-- <p><?php //echo $image['caption']; ?></p> -->
                                                <?php
                                                echo '</div>';
                                             }
                                             echo '</div>';
                                          }?>
                                 </div>
                                 <div class="project-content">
   
                                 <?php if( get_field('project-video') ): ?>
                              <h4 class="color-heading">Project Video</h4>
                              <?php endif; ?>
                              <?php if( get_field('project-video') ): ?>
                                    <a href="<?php the_field('project-video'); ?>" class="proj-link"> <img src="<?php bloginfo('template_url'); ?>/img/project-video-link-icon.svg" alt=""> <span class="video-sep"></span> Video profile link will be place over here</a>
                                    <?php endif; ?>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--end of tab Four--> 
                  </div>
                  <!--end of container-->
               </div>
            </div>
            <div class="profile-nav">
               <div class="tab-menu-wrap">
                  <ul>
                     <li><a href="javascript:;" class="tab-a active-a" data-id="tab1">Profile</a></li>
                     <li><a href="javascript:;" class="tab-a" data-id="tab2">Academic and Skills</a></li>
                     <li><a href="javascript:;" class="tab-a" data-id="tab3">Expereinces</a></li>
                     <li><a href="javascript:;" class="tab-a" data-id="tab4">Final Year Project</a></li>
                  </ul>
               </div>
               <!--end of tab-menu-->
            </div>
         </div>
      </section>
   </article>
   <!-- #post-<?php the_ID(); ?> -->
   <?php
      endwhile; // End of the loop.
      ?>
</main>
<!-- #main -->
<?php
get_footer('footer-03');