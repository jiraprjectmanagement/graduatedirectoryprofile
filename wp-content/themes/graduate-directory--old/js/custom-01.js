$('.owl-carousel').owlCarousel({
    loop:false,
    nav: false,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    dots: false,
    margin:10,
    stagePadding: 5,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav: true,
            dots:false
        },                   
        576:{
            items:1,
            nav: true,
            dots:false
        },
        768: {
            items: 2,
            dots: false,
            nav: true,
            margin:15,
        }, 
        992: {
            items: 2,
            dots: false,
            margin:15,
        }, 
        1199: {
            items: 3,
            dots: true
        }
    }
  });


  function goBack() {
    if (history.length > 2) {
          window.history.back();
  } else {
          window.location.replace("https://habib.edu.pk/graduate-directory/graduate/");
      }
  }

// single profile Share Popup
$("a.share-btn").click(function(){
    $(".share-popup").show();
    $(".share-overlay").show();
  });
  $(".share-cloes-btn").click(function(){
    $(".share-popup").hide();
    $(".share-overlay").hide();
  });