<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Habib_Graduate_Directory
 */

?>

<footer id="colophon" class="site-footer main-footer">
	<div id="footer" role="contentinfo">
	<div class="middle_footer">
            <div class="inner_middle_footer">
                <div class="footer_col left hu-col-5">
                    <div class="hu-col-10">
                        <!-- <div class="main-footer-logo">
                            <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/03/HU-logo-footer-1.svg" alt="" class="footerlogo" loading="lazy">
                        </div> -->
                        <div class="footer_adress ">
                            <h3 class="widget-title one-col">Office of Career Services</h3>
                            <p>Habib University, Block 18, Gulistan-e-Jauhar, University Avenue، Off Shahrah-e-Faisal Rd, Karachi - 75290, Sindh, Pakistan</p>
                        </div>
                        <div class="footer_uni_phone">
                            <p><a href="tel:+9221111042242">+92 21 1110 42242 (HABIB)</a></p>
                            <p><a href="mailto:career.services@habib.edu.pk">career.services@habib.edu.pk</a></p>
                        </div>
                    </div>
                </div>
                <div class="footer_col right hu-col-5">


						<div class="footer_social">
                            <h3 class="widget-title one-col">Follow Habib University</h3>
                            <a href="https://www.facebook.com/HabibUniversity" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a href="https://www.linkedin.com/company/habib-university" title="LinkedIn" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                            <a href="https://instagram.com/habibuniversity" title="Instagram" target="_blank"><i class="fab fa-instagram"></i></a> 
                            <a href="https://twitter.com/habibuniversity" title="Twitter" target="_blank"><i class="fab fa-twitter"></i></a>       
                            <a href="https://www.youtube.com/user/HabibUni" title="YouTube" target="_blank"><i class="fab fa-youtube"></i></a>
                            <a href="hhttp://habib.edu.pk/" title="website" target="_blank"><i class="fas fa-globe"></i></a>
                        </div>

                    <!--<div class="news-letter-area">
                        <h3>Quick Links</h3>
						</div> 
						<div class="footer-menu flex">
                        <div class="hu-col-6">
                            <a href="../wellness-center/physical-health"><span class="border-half-bottom">Student Physical Health Care Services</span></a>
                            <a href="../wellness-center/mental-health"><span class="border-half-bottom">Mental Health Counseling</span></a>
                            <a href="../wellness-center/student-lounge"><span class="border-half-bottom">Student Lounge</span></a>
                        </div>
                        <div class="hu-col-4">
                            <a href="../wellness-center/swimming-services"><span class="border-half-bottom">Swimming Pool</span></a>
                            <a href="../wellness-center/fitness-services"><span class="border-half-bottom">GYM</span></a>
                            <a href="../wellness-center/sports-services"><span class="border-half-bottom">Sports</span></a>
                        </div>
						</div> -->
                </div>
            </div>
        </div>
        <div class="bottom_footer">
            <div class="inner_bottom_footer">
                <div class="footer_copyright">
                    © <a href="<?php echo esc_url( __( 'https://habib.edu.pk/', 'graduate-directory' ) ); ?>"> 
                            <?php
                                /* translators: %s: CMS name, i.e. WordPress. */
                            printf( esc_html__( 'Habib University %s', 'graduate-directory' ), '- Powered by HUIT' );
                            ?>
                        </a>
                        <!-- <span class="sep"> - </span> -->
                            <?php
                                /* translators: 1: Theme name, 2: Theme author. */
                            //printf( esc_html__( 'All Rights Reserved', 'graduate-directory' ), 'graduate-directory', '' );
                            ?>
                </div>
            </div>    
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
