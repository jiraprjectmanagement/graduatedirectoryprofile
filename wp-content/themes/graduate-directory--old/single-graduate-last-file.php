<?php
   /**
    * The template for displaying all single posts
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
    *
    * @package Habib_Graduate_Directory
    */
   
   get_header('aspiration-profile'); ?>
<main id="primary" class="single site-main aspiration-profiles">
   <?php while ( have_posts() ) : the_post(); ?>
   <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <section class="profil-details-wrapper">
         <div class="container">
            <div class="profile-back">
               <a href="" class="back-btn btn-default">Back to list</a>
            </div>
            <div class="profile-details-inner">
               <div class="student-details">
                     <div class="tab-container">
                        <div class="user-detail-inner user-profile boxpad-tb">
                           <span class="prof-bg">
                              <img src="<?php bloginfo('template_url'); ?>/img/profile.jpg" alt="" class="user-img">
                           </span>
                           <div class="user-detail">
                              <h2>Abdul Wasay Usmani</h2>
                              <p>Class of 2021</p>
                              <p>BS</p>
                              <p class="program-bg">Computer Science</p>
                           </div>
                        </div>
                        <div class="watch-video-profile mobile-top-video">
                           <div class="profile-video-box">
                              <h3>Watch Video Profile</h3>
                              <div class="video-thumnail">
                                 <img src="<?php bloginfo('template_url'); ?>/img/video-thumbnail.jpg" alt="">
                                 <div class="video-icon">
                                    <a href="javascript:;">
                                    <img src="<?php bloginfo('template_url'); ?>/img/video-play-icon.svg" alt="">
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div  class="tab tab-active" data-id="tab1">
                           <div class="user-details-wraper">
                              <div class="user-detail detail-box boxpad-tb animation-layer">
                                 <h3>Aspiration Statement</h3>
                                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                              </div>
                              <div class="user-detail detail-box animation-layer br-t-res br-b-res">
                                 <h3>Contact Info</h3>
                                 <ul class="profile-social">
                                    <li>
                                       <a href="">
                                       <img src="<?php bloginfo('template_url'); ?>/img/em.svg" alt="">
                                       stdname@alumni@habib.edu.pk
                                       </a>
                                    </li>
                                    <li>
                                       <a href="">
                                       <img src="<?php bloginfo('template_url'); ?>/img/ln.svg" alt="">
                                       linkdin.com/stdname
                                       </a>	
                                    </li>
                                    <li>
                                       <a href="">
                                       <img src="<?php bloginfo('template_url'); ?>/img/pw.svg" alt="">
                                       portfoliowebsite.com
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <!--end of tab one--> 
                        <div  class="tab " data-id="tab2">
                           <div class="user-details-wraper">
                              <div class="user-detail detail-box boxpad-tb animation-layer br-b-res">
                                 <h3>Core Skills</h3>
                                 <ul class="skills-bullets">
                                    <li> <img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">C,C++ and C#</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Java</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Python</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Web Development</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">.NET</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">C, C++ and C#</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">My SQL</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">SQL</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">CSS</li>
                                 </ul>
                              </div>
                              <div class="user-detail detail-box animation-layer academic-award">
                                 <h3>Academic Awards/Achievments</h3>
                                 <ul class="skills-bullets bullet-100">
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">HU TOPS 100% Scholarship</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <!--end of tab two--> 
                        <div  class="tab " data-id="tab3">
                           <div class="user-details-wraper">
                              <div class="user-detail boxpad-tb detail-box animation-layer">
                                 <h3>Experiences</h3>
                                 <ul class="skills-bullets bullet-100">
                                    <h4>Leadership Experiences /Meta-curricular</h4>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Orientation Leader for the Batch of 2024 (2020)</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Co-Lead for Evening with Staff 2021 at Habib University</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Co-Lead for Evening with Staff 2021 at Habib University</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Director photography, Social media marketing manager and Content Writer for PridePress club at Habib University (2019)</li>
                                 </ul>
                                 <ul class="skills-bullets bullet-100">
                                    <h4>Internship / voulunteer Work</h4>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Undergraduate Researcher | Karachi Water Project (HEC Project), Habib University</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">For the Menstruator, Director Marketing – Kenya</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">Astoria Company, Content Writer, Delaware – USA</li>
                                    <li><img src="<?php bloginfo('template_url'); ?>/img/bullet-point.svg" alt="">NOWPDP, Marketing and Research Intern</li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <!--end of tab three--> 
                        <div  class="tab " data-id="tab4">
                           <div class="user-details-wraper">
                              <div class="user-detail boxpad-tb detail-box animation-layer br-t-res">
                                 <div class="project-content">
                                    <h3>Final Year Project</h3>
                                    <h4>Madadgar</h4>
                                    <p>Madadgaar is disaster relief portal that provides you basic services such as ambulance and road traffic assistance on your phone. The idea for the portal came to us after the rains that left Karachi flooded. Our aim is to provide the people of Pakistan with a solution in their time of need. Almost every other country has a service like this; It is about time Pakistan did as well</p>
                                 </div>
                                 <div class="project-content">
                                    <h4>Project pictures</h4>
                                    <div class="project-pic">
                                       <div class="proj-box">
                                          <img src="<?php bloginfo('template_url'); ?>/img/project-picture.svg" alt="">
                                       </div>
                                       <div class="proj-box">
                                          <img src="<?php bloginfo('template_url'); ?>/img/project-picture.svg" alt="">
                                       </div>
                                       <div class="proj-box">
                                          <img src="<?php bloginfo('template_url'); ?>/img/project-picture.svg" alt="">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="project-content">
                                    <h4>Project Video</h4>
                                    <a href="javascipt:;" class="proj-link"> <img src="<?php bloginfo('template_url'); ?>/img/project-video-link-icon.svg" alt=""> <span class="video-sep"></span> Video profile link will be place over here</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--end of tab Four--> 
                     </div>
                     <!--end of container-->
               </div>
               <div class="watch-video-profile desk-top-video">
                  <div class="profile-video-box">
                     <h3>Watch Video Profile</h3>
                     <div class="video-thumnail">
                        <img src="<?php bloginfo('template_url'); ?>/img/video-thumbnail.jpg" alt="">
                        <div class="video-icon">
                           <a href="javascript:;">
                           <img src="<?php bloginfo('template_url'); ?>/img/video-play-icon.svg" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="profile-nav">
               <div class="tab-menu-wrap">
                  <ul>
                     <li><a href="javascript:;" class="tab-a active-a" data-id="tab1">Profile</a></li>
                     <li><a href="javascript:;" class="tab-a" data-id="tab2">Academic and Skills</a></li>
                     <li><a href="javascript:;" class="tab-a" data-id="tab3">Expereinces</a></li>
                     <li><a href="javascript:;" class="tab-a" data-id="tab4">Final Year Project</a></li>
                  </ul>
               </div>
               <!--end of tab-menu-->
            </div>
         </div>
      </section>
   </article>
   <!-- #post-<?php the_ID(); ?> -->
   <?php
      endwhile; // End of the loop.
      ?>
</main>
<!-- #main -->
<?php
get_footer('footer-aspiration-profile');