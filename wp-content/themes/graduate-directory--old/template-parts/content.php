<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Habib_Graduate_Directory
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>><a href="<?php  echo get_post_permalink(); ?> " rel="bookmark">

	<div class="main-post-thumbnail">
		<?php 	if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				} else { ?>
					<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/04/default-featured-image.jpg" alt="<?php the_title(); ?>" />
				<?php } ?>
			<?php //graduate_directory_post_thumbnail(); ?>
	</div>

	<div class="main-post-content">
		<div class="right-post-content">
			<header class="entry-header">
				<?php
					if ( is_singular() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						// the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						the_title( '<h2 class="entry-title">', '</h2>' );
					endif;

					if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php
						graduate_directory_posted_on();
						graduate_directory_posted_by();
					?>
				</div><!-- .entry-meta -->
					<?php endif; ?>
			</header><!-- .entry-header -->
			<div class="class-name"><!-- class-name -->
					<?php
						$categories = get_the_terms( $post->ID, 'class' );
						if (is_array($categories) || is_object($categories)) {
							foreach( $categories as $category ) {
								//echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
								// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
								// echo $category->name . '<span>, </span></a>';
								echo $category->name . '<span>, </span>';
							}
						}
					?>
			</div><!-- .class-name -->
			<div class="program-name"><!-- program-name -->
				<?php
					$categories = get_the_terms( $post->ID, 'program' );
					if (is_array($categories) || is_object($categories)) {
					foreach( $categories as $category ) {
						//echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
						if(!$category->parent == 0)  {
							// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
							// echo $category->name .'<span>, </span></a>';
							echo $category->name .'<span>, </span>'; }
					}
				 } ?>
			</div><!-- program-name -->
		</div>
		<div class="left-post-content">
			<!-- <a href="<?php  echo get_post_permalink(); ?> " rel="bookmark">
					<i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 
				</a> -->
				<i class="fa fa-angle-right" aria-hidden="true"></i>
		</div>
	
	<!-- <div class="entry-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'graduate-directory' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'graduate-directory' ),
				'after'  => '</div>',
			)
		);
		?>
	</div>.entry-content -->

	</div>

	<footer class="entry-footer">
		<?php //graduate_directory_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	</a></article><!-- #post-<?php the_ID(); ?> -->
