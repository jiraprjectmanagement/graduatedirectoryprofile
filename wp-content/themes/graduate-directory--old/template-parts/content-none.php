<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Habib_Graduate_Directory
 */

?>

<section class="no-results not-found">

	<header class="page-header">
	<h1 class="page-title">Graduate <span> Directory</span></h1>
			<?php
				//the_archive_title( '<h1 class="page-title">', '</h1>' );
				//the_archive_description( '<div class="archive-description">', '</div>' );
			?>
	</header><!-- .page-header -->

<div class="main-searchandfilter">
	<div class="searchandfilter-img"><img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/1.Home_.png"/></div>
	<div class="overlay"></div>
	<div class="searchandfilter-popup">
		<span class="cloes-btn"><i class="fa fa-times" aria-hidden="true"></i></span>
		<?php echo do_shortcode( '[searchandfilter fields="search,class,program" order_dir=",asc,asc" order_by=",id,id" types=",select,select" hierarchical=",,1" headings="Search Graduate Directory,Class of,Programs" all_items_labels=",All Classes,All Programs" hide_empty=",0,0" submit_label="Apply Filters and Search" search_placeholder="By Name, Skills" empty_search_url="/graduate-directory/graduate/"]' ); ?>
	</div>
</div>





	<!-- <header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'graduate-directory' ); ?></h1>
	</header> -->

	<div class="page-content">

	<div class="no-data-found">
		<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/04/warning-sign.png"/>
		<h6>No Data Found</h6>
		<p>Please Check your filters and sorting combination in order to get proper data</p>
	</div>
		<!-- <?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'graduate-directory' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'graduate-directory' ); ?></p>
			<?php
			get_search_form();

		else :
			?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'graduate-directory' ); ?></p>
			<?php
			get_search_form();

		endif;
		?> -->
	</div>
</section><!-- .no-results -->
