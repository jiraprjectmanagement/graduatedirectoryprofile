$(document).ready(function(){
  $("#grid-view").click(function(){
    $(".main-article").removeClass("list-view");
    $(".main-article").addClass("grid-view");
  });
      $("#list-view").click(function(){
    $(".main-article").removeClass("grid-view");
    $(".main-article").addClass("list-view");
  });
});


$('.owl-carousel').owlCarousel({
  loop:false,
  nav: false,
  navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
  dots: false,
  margin:10,
  stagePadding: 5,
  autoplay:false,
  autoplayTimeout:4000,
  autoplayHoverPause:true,
  mouseDrag: true,
  responsiveClass:true,
  responsive:{
      0:{
          items:1,
          nav: true,
          dots:false
      },                   
      576:{
          items:1,
          nav: true,
          dots:false
      },
      768: {
          items: 2,
          dots: false,
          nav: true,
          margin:15,
      }, 
      992: {
          items: 2,
          dots: false,
          margin:15,
      }, 
      1199: {
          items: 3,
          dots: true
      }
  }
});


// Mobile menu Hide or Show
$("button.menu-toggle").click(function(){
  $("#nav-icon3").toggleClass("open");
  $(".menu-menu-1-container").css("width","200px");
  $(".main-searchandfilter .overlay").show();
});

$("li#menu-item-184").click(function(){
  $(".menu-menu-1-container").css("width","0px");
  $("#nav-icon3").removeClass("open");
  $(".main-searchandfilter .overlay").hide();
});

// Mobile Filter hide Or Show
$(".searchandfilter-img").click(function(){
  $(".main-searchandfilter .overlay").show();
  $(".searchandfilter-popup").addClass("popupopen");
});

$("span.cloes-btn").click(function(){
  $(".main-searchandfilter .overlay").hide();
  $(".searchandfilter-popup").removeClass("popupopen");
});


// Pagenation Style
if($('.pagelink').text() == '') {
  $(".pagelink").css("border","0px");
  $(".pagelink").css("box-shadow","none");
}
$("a.next.page-numbers").append("<img src='https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/next-arrow.svg'>");
$("a.prev.page-numbers").append("<img src='https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/previous-arrow.svg'>");

// $('.searchandfilter input[type="text"]').focus(function() {
//   $(".searchandfilter li:nth-child(1) h4").css("font-size","10px");
//   $(".searchandfilter li:nth-child(1) h4").css("transition","0.3s");
// });

// $('.searchandfilter input[type="text"]').focusout(function() {
//   $(".searchandfilter li:nth-child(1) h4").css("font-size","13px");
//   $(".searchandfilter li:nth-child(1) h4").css("transition","0.3s");
// });

// $('.searchandfilter select#ofclass').focus(function() {
//   $(".searchandfilter li:nth-child(2) h4").css("font-size","10px");
// });

// $('.searchandfilter select#ofclass').focusout(function() {
//   $(".searchandfilter li:nth-child(2) h4").css("font-size","13px");
// });

// $('.searchandfilter select#ofprogram').focus(function() {
//   $(".searchandfilter li:nth-child(3) h4").css("font-size","10px");
// });

// $('.searchandfilter select#ofprogram').focusout(function() {
//   $(".searchandfilter li:nth-child(3) h4").css("font-size","13px");
// });





  // $("#ofprogram option[value='28']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='29']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='30']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='32']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='33']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='34']").prepend('<span class="abc"></span>');
  // $(".abc").prepend('&nbsp;&nbsp;&nbsp;');

  // $('#ofprogram').on('change', function() {
  //   $('option:selected', this).attr('selected',true).siblings().removeAttr('selected');
  //   $( "#ofprogram option:selected").find('.abc').empty();
  // });
