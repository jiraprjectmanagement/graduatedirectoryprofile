<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Habib_Graduate_Directory
 */

get_header();
?>

	<main id="primary" class="cpt-post archive">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">Graduate <span> Directory</span></h1>
				<?php
			the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<div class="main-searchandfilter">
				<div class="searchandfilter-img"><img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/1.Home_.png"/></div>
				<div class="overlay"></div>
				<div class="searchandfilter-popup">
					<span class="cloes-btn"><i class="fa fa-times" aria-hidden="true"></i></span>
						<?php echo do_shortcode( '[searchandfilter fields="search,class,program" order_dir=",asc,asc" order_by=",id,id" types=",select,select" hierarchical=",,1" headings="Search Graduate Directory,Class of,Programs" all_items_labels=",All Classes,All Programs" hide_empty=",0,0" submit_label="Apply Filters and Search" search_placeholder="By Name, Skills" empty_search_url="/graduate-directory/graduate/"]' ); ?>
				</div>
			</div>

			<div class="main-page-sub-title">
				<div class="page-sub-title">
					<!-- <h3 class=""><?Php //the_archive_title( ','); ?> </h3> -->
					<h3 class="" style="font-size: 22px;">List of Graduates</h3>
				</div>
				<div class="view-icon">
					<!-- <div class="">
						<span id="grid-view"><i class="fa fa-th" aria-hidden="true"></i></span>
						<span id="list-view"><i class="fa fa-list" aria-hidden="true"></i></span>
					</div> -->
					<div id="sortby"  class="">
						<h4>Sort by: &nbsp;</h4>
						<select class="dropdown-class" name="sort-posts" id="sortbox" onchange="document.location.search=this.options[this.selectedIndex].value;">
							<option <?php if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'date' && isset($_GET["order"]) && trim($_GET["order"]) == 'DESC' ){ echo 'selected'; } ?> value="?orderby=date&order=ASC">Year</option>
							<option <?php if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'title' && isset($_GET["order"]) && trim($_GET["order"]) == 'ASC' ){ echo 'selected'; } ?>  value="?orderby=title&order=ASC">A-Z</option>
							<option <?php if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'title' && isset($_GET["order"]) && trim($_GET["order"]) == 'DESC' ){ echo 'selected'; } ?>  value="?orderby=title&order=DESC">Z-A</option>
							<!-- <option <?php//if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'date' && isset($_GET["order"]) && trim($_GET["order"]) == 'ASC' ){ echo 'selected'; } ?>  value="?orderby=date&order=ASC">Oldest</option>
        					<option <?php //if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'views' && isset($_GET["order"]) && trim($_GET["order"]) == 'ASC' ){ echo 'selected'; } ?> value="?orderby=views&order=ASC">Views Asc</option>
        					<option <?php //if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'views' && isset($_GET["order"]) && trim($_GET["order"]) == 'DESC' ){ echo 'selected'; } ?> value="?orderby=views&order=DESC">Views Desc</option> -->
						</select>
					</div>
				</div>
			</div>


			<!-- <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
	<?php
		if( $terms = get_terms( array( 'taxonomy' => 'class', 'orderby' => 'date' ) ) ) : 
	
			echo '<select name="classcategoryfilter"><option value="">Select category...</option>';
			foreach ( $terms as $term ) :
				echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
			endforeach;
			echo '</select>';
		endif;
	?>
		<?php
		if( $terms = get_terms( array( 'taxonomy' => 'program', 'orderby' => 'date' ) ) ) : 
	
			echo '<select name="categoryfilter"><option value="">Select category...</option>';
			foreach ( $terms as $term ) :
				echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
			endforeach;
			echo '</select>';
		endif;
	?> 
	<!===-- <input type="text" name="price_min" placeholder="Min price" />
	<input type="text" name="price_max" placeholder="Max price" /> --=====>
	<label>
		<input type="radio" name="date" value="ASC" /> Date: Ascending
	</label>
	<label>
		<input type="radio" name="date" value="DESC" selected="selected" /> Date: Descending
	</label>
	<!=====-- <label>
		<input type="checkbox" name="featured_image" /> Only posts with featured images
	</label> ---===>
	<button>Apply filter</button>
	<input type="hidden" name="action" value="myfilter">
</form>
<div id="response"></div> -->

<!-- <script>
	jQuery(function($){
	jQuery('#filter').submit(function(){
		var filter = jQuery('#filter');
		$.ajax({
			url:filter.attr('action'),
			data:filter.serialize(), // form data
			type:filter.attr('method'), // POST
			beforeSend:function(xhr){
				filter.find('button').text('Processing...'); // changing the button label
			},
			success:function(data){
				filter.find('button').text('Apply filter'); // changing the button label back
				jQuery('#response').html(data); // insert data
			}
		});
		return false;
	});
});
</script> -->



			<div class="main-article grid-view"> <!--- list-view --->
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */

				// get_template_part( 'template-parts/content', get_post_type() ); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>><a href="<?php  echo get_post_permalink(); ?> " rel="bookmark">
					
					<div class="main-post-thumbnail">
					<?php 	if ( has_post_thumbnail() ) {
								the_post_thumbnail();
							} else { ?>
								<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/05/no-user-picture.png" alt="<?php the_title(); ?>" />
							<?php } ?>
						<?php //graduate_directory_post_thumbnail(); ?>
					</div>

					<div class="main-post-content">
						<div class="right-post-content">
					<header class="entry-header">
						<?php
							if ( is_singular() ) :
								the_title( '<h1 class="entry-title">', '</h1>' );
							else :
								// the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
								the_title( '<h2 class="entry-title">', '</h2>' );
							endif;

							if ( 'post' === get_post_type() ) :
						?>
						<div class="entry-meta">
							<?php
								graduate_directory_posted_on();
								graduate_directory_posted_by();
							?>
						</div><!-- .entry-meta -->
						<?php endif; ?>
					</header><!-- .entry-header -->

					<div class="class-name"><!-- class-name -->
						<?php
							$categories = get_the_terms( $post->ID, 'class' );
							if (is_array($categories) || is_object($categories)) {
								foreach( $categories as $category ) {
									//echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
									// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
									// echo $category->name . '<span>, </span></a>';
									echo $category->name . '<span>, </span>';
								}
							 } ?>
					</div><!-- .class-name -->

					<div class="program-name"><!-- program-name -->

							<?php
							$categories = get_the_terms( $post->ID, 'program' );
							if (is_array($categories) || is_object($categories)) {
								foreach( $categories as $category ) {
									//echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
									if(!$category->parent == 0)  {
									// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
									// echo $category->name .'<span>, </span></a>';
									echo $category->name .'<span>, </span>'; }
								} 
							}?>


						 	<!-- <?php
   								$args = array(
               							'taxonomy' => 'program',
               							'orderby' => 'name',
               							'order'   => 'ASC',
			   							'hide_empty' => 1,
       									'hierarchical' => 1,
			   							'child_of' => 0,
			   							'parent'=> ''
           							);

   								$cats = get_categories($args);

								   print_r ($cats);

   								foreach($cats as $cat) {
							?>
							<?php if(!$cat->category_parent == 0)  {?>

      							<a class="" href="<?php echo get_category_link( $cat->term_id ) ?>">
           							<?php echo $cat->name; ?>
      							</a>
							<?php
   									}
  								}
							?> -->
					</div><!-- .program-name -->
							</div>
							<div class="left-post-content">
								<!-- <a href="<?php  echo get_post_permalink(); ?> " rel="bookmark">
									<i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 
								</a> -->
								<i class="fa fa-angle-right" aria-hidden="true"></i>
							</div>


					<!-- <div class="entry-content">
						<?php
							the_content(
								sprintf(
									wp_kses(
									__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'graduate-directory' ),
										array(
											'span' => array(
											'class' => array(),
											),
										)
									),
								wp_kses_post( get_the_title() )
								)
							);

							wp_link_pages(
								array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'graduate-directory' ),
									'after'  => '</div>',
								)
							);
						?>

					</div>==== .entry-content ====

					<footer class="entry-footer">
						<?php graduate_directory_entry_footer(); ?>
					</footer>==== .entry-footer ==== -->
					</div>
					</a></article><!-- #post-<?php the_ID(); ?> -->

		<?php	endwhile;

			//the_posts_navigation();

			?><div class="main-pagelink"><div class="pagelink"><?php echo paginate_links(); ?></div></div><?php

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
			</div>
	</main><!-- #main -->

<?php
// get_sidebar();
get_footer();
