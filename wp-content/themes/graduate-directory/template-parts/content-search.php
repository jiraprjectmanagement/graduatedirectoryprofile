<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Habib_Graduate_Directory
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>><a href="<?php  echo get_post_permalink(); ?> " rel="bookmark">

	<div class="main-post-thumbnail">
			<?php 	if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				} else { ?>
					<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/05/no-user-picture.png" alt="<?php the_title(); ?>" />
				<?php } ?>
			<?php //graduate_directory_post_thumbnail(); ?>
	</div>

	<div class="main-post-content">
		<div class="right-post-content">
			<header class="entry-header">
				<?php
					if ( is_singular() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						// the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						// the_title( '<h2 class="entry-title">', '</h2>' );
						$string = get_the_title();
						$array = explode(" ", $string);
						echo '<h2 class="entry-title">';					
						if(!empty($array[0])){ echo $array[0]; } else { echo ""; }
						echo " ";
						if(!empty($array[1])){ echo $array[1]; } else { echo ""; }
						echo " ";
						if(!empty($array[2])){ echo $array[2]; } else { echo ""; }
						echo "</h2>";
					endif;

					if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php
						graduate_directory_posted_on();
						graduate_directory_posted_by();
					?>
				</div><!-- .entry-meta -->
					<?php endif; ?>
			</header><!-- .entry-header -->
			<div class="class-name"><!-- class-name -->
					<?php
						$categories = get_the_terms( $post->ID, 'class' );
						if (is_array($categories) || is_object($categories)) {
							foreach( $categories as $category ) {
								//echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
								// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
								// echo $category->name . '<span>, </span></a>';
								echo $category->name . '<span>, </span>';
							}
					}
					?>
			</div><!-- .class-name -->
			<div class="program-name"><!-- program-name -->
				<?php
					$categories = get_the_terms( $post->ID, 'program' );
					if (is_array($categories) || is_object($categories)) {
					foreach( $categories as $category ) {
						//echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
						if(!$category->parent == 0)  {
							// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
							// echo $category->name .'<span>, </span></a>';
							echo $category->name .'<span>, </span>'; }
					}
				 	} ?>
			</div><!-- program-name -->
		</div>
		<div class="left-post-content">
			<!-- <a href="<?php  echo get_post_permalink(); ?> " rel="bookmark">
					<i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 
				</a> -->
				<i class="fa fa-angle-right" aria-hidden="true"></i>
		</div>
	</div>



	<!-- <header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php
			graduate_directory_posted_on();
			graduate_directory_posted_by();
			?>
		</div>
		<?php endif; ?>
	</header>

	<?php graduate_directory_post_thumbnail(); ?>

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div> -->

	<footer class="entry-footer">
		<?php //graduate_directory_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	</a></article><!-- #post-<?php the_ID(); ?> -->
