<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Habib_Graduate_Directory
 */

?>

<header class="page-header">
	<h1 class="page-title">Graduate <span> Directory</span></h1>
		<!-- <?php
			the_archive_title( '<h1 class="page-title">', '</h1>' );
			the_archive_description( '<div class="archive-description">', '</div>' );
		?> -->
</header><!-- .page-header -->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class="main-searchandfilter">
	<div class="searchandfilter-img"><img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/1.Home_.png"/></div>
	<div class="overlay"></div>
	<div class="searchandfilter-popup">
		<span class="cloes-btn"><i class="fa fa-times" aria-hidden="true"></i></span>
		<?php echo do_shortcode( '[searchandfilter fields="search,class,program" order_dir=",asc,asc" order_by=",date,date" types=",checkbox,checkbox" hierarchical=",,1" headings="Search Graduate Directory,Class of,Programs" all_items_labels=",All Classes,All Programs" hide_empty=",0,0" operators=",OR,OR" submit_label="Search" search_placeholder="By Name, Skills" empty_search_url="/graduate-directory/graduate/"]' ); ?>
		<?php //echo do_shortcode( '[searchandfilter fields="search,class,program" order_dir=",asc,asc" order_by=",id,id" types=",select,select" hierarchical=",,1" headings="Search Graduate Directory,Class of,Programs" all_items_labels=",All Classes,All Programs" hide_empty=",0,0" submit_label="Search" search_placeholder="By Name, Skills" empty_search_url="/graduate-directory/graduate/"]' ); ?>
	</div>
</div>

	<!-- <header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header> 

	<?php graduate_directory_post_thumbnail(); ?> -->

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'graduate-directory' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<?php  if ( get_edit_post_link() ) : ?>
		<!-- <footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'graduate-directory' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer> -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
