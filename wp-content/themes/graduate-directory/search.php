<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Habib_Graduate_Directory
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>


			<header class="page-header">
				<h1 class="page-title">Graduates Directory</h1>
				<?php
				//the_archive_title( '<h1 class="page-title">', '</h1>' );
				//the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<div class="main-searchandfilter">
				<div class="searchandfilter-img"><img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/1.Home_.png"/></div>
				<div class="overlay"></div>
				<div class="searchandfilter-popup">
					<span class="cloes-btn"><i class="fa fa-times" aria-hidden="true"></i></span>
						<?php echo do_shortcode( '[searchandfilter fields="search,class,program" order_dir=",asc,asc" order_by=",date,date" types=",checkbox,checkbox" hierarchical=",,1" headings="Search Graduate Directory,Class of,Programs" all_items_labels=",All Classes,All Programs" hide_empty=",0,0" operators=",OR,OR" submit_label="Search" search_placeholder="By Name, Skills" empty_search_url="/graduate-directory/graduate/"]' ); ?>
						<?php //echo do_shortcode( '[searchandfilter fields="search,class,program" order_dir=",asc,asc" order_by=",id,id" types=",select,select" hierarchical=",,1" headings="Search Graduate Directory,Class of,Programs" all_items_labels=",All Classes,All Programs" hide_empty=",0,0" submit_label="Search" search_placeholder="By Name, Skills" empty_search_url="/graduate-directory/graduate/"]' ); ?>
				</div>
			</div>


			<div class="main-page-sub-title">
				<div class="page-sub-title">
				<p class="jqury-url" style="display: none;"></p>
				<h3 class="jqury-titel page-title" style="font-size: 14px!important;"><span class="first">Search Results of: </span><span><?php	printf( esc_html__( ' %s', 'graduate-directory' ),  get_search_query() ); 	?> </span></h3>
					<h3 class=""><?php//printf( esc_html__( 'Search Results for: %s', 'graduate-directory' ), '<span>' . get_search_query() . '</span>' ); 	?> </h3>
				</div>
				<div class="view-icon">
					<!-- <div class="">
						<span id="grid-view"><i class="fa fa-th" aria-hidden="true"></i></span>
						<span id="list-view"><i class="fa fa-list" aria-hidden="true"></i></span>
					</div> -->
					<div id="sortby"  class="">
						<!-- <h4>SORT BY: &nbsp;</h4>
        				<select class="dropdown-class" name="sort-posts" id="sortbox" onchange="document.location.search=this.options[this.selectedIndex].value;">
        					<option disabled>Sort by</option>
        					<option <?php if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'date' && isset($_GET["order"]) && trim($_GET["order"]) == 'DESC' ){ echo 'selected'; } ?> value="?orderby=date&order=DESC">Newest</option>
        					<option <?php if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'date' && isset($_GET["order"]) && trim($_GET["order"]) == 'ASC' ){ echo 'selected'; } ?>  value="?orderby=date&order=ASC">Oldest</option>
        					<option <?php if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'title' && isset($_GET["order"]) && trim($_GET["order"]) == 'ASC' ){ echo 'selected'; } ?>  value="?orderby=title&order=ASC">A-Z Asc</option>
        					<option <?php if( isset($_GET["orderby"]) && trim($_GET["orderby"]) == 'title' && isset($_GET["order"]) && trim($_GET["order"]) == 'DESC' ){ echo 'selected'; } ?>  value="?orderby=title&order=DESC">A-Z Desc</option>
        				</select> -->
        			</div>
				</div>
			</div>


			<div class="main-article grid-view">
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			//the_posts_navigation();
			?><div class="main-pagelink"><div class="pagelink"><?php echo paginate_links(); ?></div></div><?php

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
			</div>
	</main><!-- #main -->

<?php
//get_sidebar();
get_footer();
