// slim-scroll initialization 
if($(window).width() >= 768){
  if(('.serv-responsive-slider').length != 0){
    $(".tab-scroll").slimScroll({ 
      height: "375px",
      color: "#c5c5c5"
    }); 
  }
}

// Single Perofil Back Button
function goBack() {
  if (history.length > 2) {
		window.history.back();
} else {
		window.location.replace("https://habib.edu.pk/graduate-directory/graduate/");
    }
}

//Mobile Menu Open and Cloes
$(".graduate-template-single-graduate-03 #menu-item-165 a").attr("href", "javascript:;");
$("button.menu-toggle").click(function(){
  $("#nav-icon3").toggleClass("open");
});

//Mobile Search and Filter Popup
$("span.cloes-btn").click(function(){
  $(".searchandfilter-popup").hide();
  $(".main-searchandfilter .overlay").hide();
});

$(".searchandfilter-img").click(function(){
  $(".searchandfilter-popup").show();
  $(".main-searchandfilter .overlay").show();
});


// Tabs
$('.tab-a').click(function(){  
  $(".tab").removeClass('tab-active');
  $(".tab[data-id='"+$(this).attr('data-id')+"']").addClass("tab-active");
  $(".tab-a").removeClass('active-a');
  $(this).parent().find(".tab-a").addClass('active-a');
});

$('.tab-menu-wrap ul li:nth-child(1) a').click(function(){
 $('[data-id="tab1"] .animation-layer:nth-child(1)').addClass('animleftlayerone')
 $('[data-id="tab1"] .animation-layer:nth-child(2)').addClass('animleftlayertwo')
 $('[data-id="tab1"] .animation-layer:nth-child(3)').addClass('animleftlayerthree')
//  $('.user-profile').removeClass('user-prof-anim');
});
$('.tab-menu-wrap ul li:nth-child(2) a').click(function(){
  $('[data-id="tab2"] .animation-layer:nth-child(1)').addClass('animleftlayerupone')
  $('[data-id="tab2"] .animation-layer:nth-child(2)').addClass('animleftlayeruptwo')
  // $('.user-profile').addClass('user-prof-anim');
});
$('.tab-menu-wrap ul li:nth-child(3) a').click(function(){
  $('[data-id="tab3"] .animation-layer:nth-child(1)').addClass('animleftlayerupone')
  // $('.user-profile').addClass('user-prof-anim');
});
$('.tab-menu-wrap ul li:nth-child(4) a').click(function(){
  $('[data-id="tab4"] .animation-layer:nth-child(1)').addClass('animleftlayerupone')
  // $('.user-profile').addClass('user-prof-anim');
});
  

// Mobile menu Hide or Show
$(".menu-toggle").click(function(){
  $("#nav-icon3").toggleClass("open");
  $(".menu-menu-1-container").css("width","200px");
  $(".main-searchandfilter .overlay").show();
});
$("li#menu-item-184").click(function(){
  $(".menu-menu-1-container").css("width","0px");
  $("#nav-icon3").removeClass("open");
  $(".main-searchandfilter .overlay").hide();
});


// Contact us Popup
$("li#menu-item-165 a").click(function(){
  $(".custom-popup").css({"opacity":"1", "pointer-events": "initial"});
  $(".share-overlay").show();
});
$(".cross-close").click(function(){
  $(".custom-popup").css({"opacity":"0", "pointer-events": "none"});
  $(".share-overlay").hide();
});

//page loader
$(window).on('load', function(){
  setTimeout(removeLoader, 1000); //wait for page load PLUS two seconds.
});
function removeLoader(){
    $( "#page-overlay" ).fadeOut(1000, function() {
      // fadeOut complete. Remove the loading div
      $( "#page-overlay" ).remove(); //makes page more lightweight 
  });  
}

// page loader add class after time
$('h1.loading-title span').each(function(i, el) {
  setTimeout(function() {
      $(el).addClass('black');
  }, i * 500);
});

//Share Button
$( ".share-btn" ).click(function() {
  $( ".main-share" ).toggle( "slide" );
});


// single profile Share Popup
// $("a.share-btn").click(function(){
//   $(".share-popup").css({"opacity":"1", "pointer-events": "initial"});
//   $(".share-overlay").show();
// });
// $(".share-cloes-btn").click(function(){
//   $(".share-popup").css({"opacity":"0", "pointer-events": "none"});
//   $(".share-overlay").hide();
// });


// $('.menu-toggle').click(function(){
//     $(".menu-menu-1-container").toggleClass("show");
// })