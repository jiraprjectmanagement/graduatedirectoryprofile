//Change Logo Link 
jQuery(document).ready(function($){  
    $(".logo a.custom-logo-link").attr("href", "https://habib.edu.pk/");
  })

//Owl Carousel
$('.owl-carousel').owlCarousel({
    loop:false,
    nav: false,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    dots: false,
    margin:10,
    stagePadding: 5,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav: true,
            dots:false
        },                   
        576:{
            items:1,
            nav: true,
            dots:false
        },
        768: {
            items: 2,
            dots: false,
            nav: true,
            margin:15,
        }, 
        992: {
            items: 2,
            dots: false,
            margin:15,
        }, 
        1199: {
            items: 3,
            dots: true
        }
    }
  });

// Single Perofil Back Button
function goBack() {
    if (history.length > 2) {
          window.history.back();
  } else {
          window.location.replace("https://habib.edu.pk/graduate-directory/graduate/");
      }
  }

// page loader
 $(window).on('load', function(){
    setTimeout(removeLoader, 3000); //wait for page load PLUS two seconds.
  });
  function removeLoader(){
      $( "#page-overlay" ).fadeOut(3000, function() {
        // fadeOut complete. Remove the loading div
        $( "#page-overlay" ).remove(); //makes page more lightweight 
    });  
  }

 // page loader add class after time
$('h1.loading-title span').each(function(i, el) {
    setTimeout(function() {
        $(el).addClass('black');
    }, i * 500);
  });
  
  // Share Button Add Slider Class
if($(window).width() >= 700){
    $( ".share-btn" ).click(function() {
        $( ".main-share" ).toggleClass("slide");
      });    
}
$( ".share-btn" ).click(function() {
  $( ".main-share" ).toggleClass("slide-responsive");
});


  
// single profile Share Popup
// $("a.share-btn").click(function(){
//     $(".share-popup").show();
//     $(".share-overlay").show();
//   });
//   $(".share-cloes-btn").click(function(){
//     $(".share-popup").hide();
//     $(".share-overlay").hide();
//   });





$($('.widthmeasure')).each(function(){// id of ul
    var li = $(this).find('li')//get each li in ul
    li.each(function(){// id of ul
        // alert( $(this).width() );
        var getwidth = $(this).width() >= 500;
      if(getwidth){
        $(this).addClass("width100");
      }
      else{
        $(this).addClass("width50");
      }
    });
});
// $('.nav-links div a').addClass("gardi-dflt");
// $('.nav-links div a span').wrap("<div class='over-area'></div>)");
// $('.nav-links .nav-previous a span').attr("data-hover", "Previous");
// $('.nav-links .nav-next a span').attr("data-hover", "Next");
