//Change Logo Link 
jQuery(document).ready(function($){  
  $(".logo a.custom-logo-link").attr("href", "https://habib.edu.pk/");
})


// Change Grid View to List Viwe
$(document).ready(function(){
  $("#grid-view").click(function(){
    $(".main-article").removeClass("list-view");
    $(".main-article").addClass("grid-view");
  });
      $("#list-view").click(function(){
    $(".main-article").removeClass("grid-view");
    $(".main-article").addClass("list-view");
  });
});

// Owl Carousel All Over Website
$('.owl-carousel').owlCarousel({
  loop:false,
  nav: false,
  navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
  dots: false,
  margin:10,
  stagePadding: 5,
  autoplay:false,
  autoplayTimeout:4000,
  autoplayHoverPause:true,
  mouseDrag: true,
  responsiveClass:true,
  responsive:{
      0:{
          items:1,
          nav: true,
          dots:false
      },                   
      576:{
          items:1,
          nav: true,
          dots:false
      },
      768: {
          items: 2,
          dots: false,
          nav: true,
          margin:15,
      }, 
      992: {
          items: 2,
          dots: false,
          margin:15,
      }, 
      1199: {
          items: 3,
          dots: true
      }
  }
});

// Mobile Menu Hide or Show
$("button.menu-toggle").click(function(){
  $("#nav-icon3").toggleClass("open");
  $(".menu-menu-1-container").css("width","200px");
  $(".main-searchandfilter .overlay").show();
});
$("li#menu-item-184").click(function(){
  $(".menu-menu-1-container").css("width","0px");
  $("#nav-icon3").removeClass("open");
  $(".main-searchandfilter .overlay").hide();
});

// Mobile Filter hide Or Show
$(".searchandfilter-img").click(function(){
  $(".main-searchandfilter .overlay").show();
  $(".searchandfilter-popup").addClass("popupopen");
});
$("span.cloes-btn").click(function(){
  $(".main-searchandfilter .overlay").hide();
  $(".searchandfilter-popup").removeClass("popupopen");
});

// Pagenation Style
if($('.pagelink').text() == '') {
  $(".pagelink").css("border","0px");
  $(".pagelink").css("box-shadow","none");
}
$("a.next.page-numbers").append("<img src='https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/next-arrow.svg'>");
$("a.prev.page-numbers").append("<img src='https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/previous-arrow.svg'>");

// page loader
$(window).on('load', function(){
  setTimeout(removeLoader, 1000); //wait for page load PLUS two seconds.
});
function removeLoader(){
    $( "#page-overlay" ).fadeOut(1000, function() {
      $( "#page-overlay" ).remove(); 
  });  
}


// $('.searchandfilter input[type="text"]').focus(function() {
//   $(".searchandfilter li:nth-child(1) h4").css("font-size","10px");
//   $(".searchandfilter li:nth-child(1) h4").css("transition","0.3s");
// });

// $('.searchandfilter input[type="text"]').focusout(function() {
//   $(".searchandfilter li:nth-child(1) h4").css("font-size","13px");
//   $(".searchandfilter li:nth-child(1) h4").css("transition","0.3s");
// });

// $('.searchandfilter select#ofclass').focus(function() {
//   $(".searchandfilter li:nth-child(2) h4").css("font-size","10px");
// });

// $('.searchandfilter select#ofclass').focusout(function() {
//   $(".searchandfilter li:nth-child(2) h4").css("font-size","13px");
// });

// $('.searchandfilter select#ofprogram').focus(function() {
//   $(".searchandfilter li:nth-child(3) h4").css("font-size","10px");
// });

// $('.searchandfilter select#ofprogram').focusout(function() {
//   $(".searchandfilter li:nth-child(3) h4").css("font-size","13px");
// });


  // $("#ofprogram option[value='28']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='29']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='30']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='32']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='33']").prepend('<span class="abc"></span>');
  // $("#ofprogram option[value='34']").prepend('<span class="abc"></span>');
  // $(".abc").prepend('&nbsp;&nbsp;&nbsp;');

  // $('#ofprogram').on('change', function() {
  //   $('option:selected', this).attr('selected',true).siblings().removeAttr('selected');
  //   $( "#ofprogram option:selected").find('.abc').empty();
  // });


  
  
// Search DropDown
$(".searchandfilter ul ul").addClass("dropdown-order");
$(".cat-item-27 .children, .cat-item-31 .children").removeClass("dropdown-order");
$(".dropdown-order").parent('li').addClass("dropdown-list");
$(".dropdown-order .cat-item label").append("<span class='label-span'></span>");
$(".searchandfilter ul:first-child li.dropdown-list:nth-child(2)").addClass("ClassOff");
$(".searchandfilter ul:first-child li.dropdown-list:nth-child(3)").addClass("ProgramOff");
$(".ClassOff").siblings().addClass("main-items");
$(".ClassOff").addClass("main-items");

$(".ClassOff h4").append("<span class='span-all'>All Classes</span>");
$(".ProgramOff h4").append("<span class='span-all'>All Programs</span>");

// $('.cat-item input').change(function(){
//   if($(this).is(":checked")) {
//     $(this).parent("label").addClass("item-marked")
//   }
//   else {
//     $(this).parent("label").removeClass("item-marked")
// }
// });

// $('.dropdown-order li label input[type=checkbox]').attr('id', function(i) {
//   return 'order'+(i+1);
// });

// $(".cat-item-27 input[type=checkbox]:eq(0)").change(function(){
//   if($(this).is(":checked")) {
//     $(".cat-item-27 .children input[type=checkbox]").prop("checked", true);
//     $(".cat-item-27 .children .cat-item label").removeClass("item-marked");
//     $(".cat-item-27 .children label").css({"color":"#000"});
//   }
//   else {
//     $(".cat-item-27 .children input[type=checkbox]").prop("checked", false);
//     $(".cat-item-27 .children label").css({"color":"#afafaf"});
//   }
// });
// $(".cat-item-31 input[type=checkbox]:eq(0)").change(function(){
//   if($(this).is(":checked")) {
//     $(".cat-item-31 .children input[type=checkbox]").prop("checked", true);
//     $(".cat-item-31 .children .cat-item label").removeClass("item-marked");
//     $(".cat-item-31 .children label").css({"color":"#000"});
//   }
//   else {
//     $(".cat-item-31 .children input[type=checkbox]").prop("checked", false);
//     $(".cat-item-31 .children label").css({"color":"#afafaf"});
//   }
// });


// var count = $(".cat-item-27 .children input[type=checkbox]").length;
// alert(count);

// DSS Dropdown Parent checkbox click checked All Child and chnage Color
$('.cat-item-27 input[type=checkbox]:eq(0)').on('click', function() {
  $('.cat-item-27 .children input[type=checkbox]').prop('checked', this.checked);
});
$('.cat-item-27 .children input[type=checkbox]').on('click', function(){
  $('.cat-item-27 input[type=checkbox]:eq(0)').prop('checked', false);
});
$('.cat-item-27 .children input[type=checkbox]').on('click', function() {
if(($('.cat-item-27 .children input[type=checkbox]:checked').length) == ($('.cat-item-27 .children input[type=checkbox]').length))
{  $(".cat-item-27 input[type=checkbox]:eq(0)").prop('checked', true); } 
});

$('.cat-item-27 input[type=checkbox]').click(function(){
  $('.cat-item-27 input[type=checkbox]').each(function() {
    if($(this).is(":checked")) {
      $(this).parent().css({"color":"#000"});
  }
  else {
    $(this).parent().css({"color":"#afafaf"});
}
});
});

// AHSSS Dropdown Parent checkbox click checked All Child and chnage Color
$('.cat-item-31 input[type=checkbox]:eq(0)').on('click', function() {
$('.cat-item-31 .children input[type=checkbox]').prop('checked', this.checked);
});
$('.cat-item-31 .children input[type=checkbox]').on('click', function(){
$('.cat-item-31 input[type=checkbox]:eq(0)').prop('checked', false);
});
$('.cat-item-31 .children input[type=checkbox]').on('click', function() {
if(($('.cat-item-31 .children input[type=checkbox]:checked').length) == ($('.cat-item-31 .children input[type=checkbox]').length))
{  $(".cat-item-31 input[type=checkbox]:eq(0)").prop('checked', true); } 
});

$('.cat-item-31 input[type=checkbox]').click(function(){
$('.cat-item-31 input[type=checkbox]').each(function() {
  if($(this).is(":checked")) {
    $(this).parent().css({"color":"#000"});
}
else {
  $(this).parent().css({"color":"#afafaf"});
}
});
});

  $(".ClassOff").click(function(){
    $(".ClassOff  .dropdown-order").toggle();
    $(".ProgramOff  .dropdown-order").hide();
    $(this).toggleClass("rotate-arow");
 });
 $(".ProgramOff").click(function(){
   $(".ProgramOff  .dropdown-order").toggle();
   $(".ClassOff  .dropdown-order").hide();
   $(".ClassOff:after").css({"transform":"rotate(0deg)"});
   $(this).toggleClass("rotate-arow");
 });




// Append text on Chnage Program
//DSSE
$(".cat-item-27 [type=checkbox]:eq(0)").change(function(){
  if($(this).is(":checked")) {
    $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""))
    $(".ProgramOff .span-all").append("BSCE, BSCS, BSEE, ");
  }
  else {
      $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("BSCE, BSCS, BSEE, ", ""));
  }
});
$(".cat-item-28 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
    $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""))
    $(".ProgramOff .span-all").append("BSCE, ");
  }
  else {
      $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("BSCE, ", ""));
  }
});
$(".cat-item-29 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
    $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""))
    $(".ProgramOff .span-all").append("BSCS, ");
  }
  else {
      $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("BSCS, ", ""));
  }
});
$(".cat-item-30 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
    $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""))
    $(".ProgramOff .span-all").append("BSEE, ");
  }
  else {
      $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("BSEE, ", ""));
  }
});
// AHSS
$(".cat-item-31 [type=checkbox]:eq(0)").change(function(){
  if($(this).is(":checked")) {
    $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""))
    $(".ProgramOff .span-all").append("BA CND, BA CH, BSc SDP, ");
  }
  else {
      $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("BA CND, BA CH, BSc SDP, ", ""));
  }
});
$(".cat-item-32 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
    $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""))
    $(".ProgramOff .span-all").append("BA CND, ");
  }
  else {
      $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("BA CND, ", ""));
  }
});
$(".cat-item-33 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
    $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""))
    $(".ProgramOff .span-all").append("BA CH, ");
  }
  else {
      $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("BA CH, ", ""));
  }
});
$(".cat-item-34 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
    $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""))
    $(".ProgramOff .span-all").append("BSc SDP, ");
  }
  else {
      $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("BSc SDP, ", ""));
  }
});

// .val();
$('.ProgramOff [type=checkbox]').change(function () {
  if (!$('.ProgramOff [type=checkbox]:checked').length == $('.ProgramOff [type=checkbox]').length){
      $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""))
   }
   else if ($('.ProgramOff [type=checkbox]:checked').length <= 0) {
      //$(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", "All Classes"));
       $(".ProgramOff .span-all").text("All Programs");
   }
  });


// Append text on Chnage classes
$(".cat-item-15 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
    $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""))
    $(".ClassOff .span-all").append("2022, "); }
  else { $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("2022, ", "")); }
});
$(".cat-item-14 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
  $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""))
  $(".ClassOff .span-all").append("2021, "); }
  else { $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("2021, ", "")); }
});
$(".cat-item-13 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
  $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""))
  $(".ClassOff .span-all").append("2020, "); }
  else { $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("2020, ", "")); }
});
$(".cat-item-35 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
  $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""))
  $(".ClassOff .span-all").append("2019, "); }
  else { $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("2019, ", "")); }
});
$(".cat-item-37 [type=checkbox]").change(function(){
  if($(this).is(":checked")) {
  $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""))
  $(".ClassOff .span-all").append("2018, "); }
  else { $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("2018, ", "")); }
});

// .val();
$('.ClassOff [type=checkbox]').change(function () {
  if (!$('.ClassOff [type=checkbox]:checked').length == $('.ClassOff [type=checkbox]').length){
      $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""))
   }
   else if ($('.ClassOff [type=checkbox]:checked').length <= 0) {
       $(".ClassOff .span-all").text("All Classes");
   }
  });

//   //append text on load classes
//   jQuery(document).ready(function($){   
// if ($(".jqury-url").is(':contains("class-of-2022")')) {
//       $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
//       $(".ClassOff .span-all").append("2022, ");}
// if ($(".jqury-url").is(':contains("class-of-2021")')) {
//       $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
//       $(".ClassOff .span-all").append("2021, ");
//     alert("sss");}
// if ($(".jqury-url").is(':contains("class-of-2020")')) {
//       $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
//       $(".ClassOff .span-all").append("2020, ");}
// if ($(".jqury-url").is(':contains("class-of-2019")')) {
//       $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
//       $(".ClassOff .span-all").append("2019, ");}
// if ($(".jqury-url").is(':contains("class-of-2018")')) {
//       $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
//       $(".ClassOff .span-all").append("2018, ");}
//       });




// //////////////////////////////////////////////////////////////////////////////////////
// archive Pgae Js
////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function(){
  var pageURL = $(location).attr('href');
   $(".jqury-url").append(pageURL);
  
    //append text on load classes
  if ($(".jqury-url").is(':contains("class-of-2022")')) {
        $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
        $(".ClassOff .span-all").append("2022, ");}
  if ($(".jqury-url").is(':contains("class-of-2021")')) {
        $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
        $(".ClassOff .span-all").append("2021, "); }
  if ($(".jqury-url").is(':contains("class-of-2020")')) {
        $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
        $(".ClassOff .span-all").append("2020, ");}
  if ($(".jqury-url").is(':contains("class-of-2019")')) {
        $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
        $(".ClassOff .span-all").append("2019, ");}
  if ($(".jqury-url").is(':contains("class-of-2018")')) {
        $(".ClassOff .span-all").text($(".ClassOff .span-all").text().replace("All Classes", ""));
        $(".ClassOff .span-all").append("2018, ");}
  
  if ($(".jqury-url").is(':contains("dsse")')) {
        $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""));
        $(".ProgramOff .span-all").append("DSSE, ");}
  if ($(".jqury-url").is(':contains("bsce")')) {
        $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""));
        $(".ProgramOff .span-all").append("BSCE, "); }
  if ($(".jqury-url").is(':contains("bscs")')) {
        $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""));
        $(".ProgramOff .span-all").append("BSCS, ");}
  if ($(".jqury-url").is(':contains("bsee")')) {
        $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""));
        $(".ProgramOff .span-all").append("BSEE, ");}
  
  if ($(".jqury-url").is(':contains("ahss")')) {
        $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""));
        $(".ProgramOff .span-all").append("AHSS, ");}
  if ($(".jqury-url").is(':contains("cnd")')) {
        $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""));
        $(".ProgramOff .span-all").append("CND, ");}
  if ($(".jqury-url").is(':contains("ch")')) {
        $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""));
        $(".ProgramOff .span-all").append("CH, ");}
  if ($(".jqury-url").is(':contains("sdp")')) {
        $(".ProgramOff .span-all").text($(".ProgramOff .span-all").text().replace("All Programs", ""));
        $(".ProgramOff .span-all").append("SDP, ");}
  
  
   $(".jqury-url").each(function () {
    if ($(this).is(':contains("class-of-2022")'))
    { $(".jqury-titel").append("<span>Class of 2022</span>"); }
    if ($(this).is(':contains("class-of-2021")'))
    { $(".jqury-titel").append("<span>Class of 2021</span>"); }
    if ($(this).is(':contains("class-of-2020")'))
    { $(".jqury-titel").append("<span>Class of 2020</span>"); }
    if ($(this).is(':contains("class-of-2019")'))
    { $(".jqury-titel").append("<span>Class of 2019</span>"); }
    if ($(this).is(':contains("class-of-2018")'))
    { $(".jqury-titel").append("<span>Class of 2018</span>"); }
  
    if ($(this).is(':contains("dsse")'))
    { $(".jqury-titel").append("<span>Dhanani School of Science and Engineering</span>"); }
    if ($(this).is(':contains("bsce")'))
    { $(".jqury-titel").append("<span>BS Computer Engineering</span>"); }
    if ($(this).is(':contains("bscs")'))
    { $(".jqury-titel").append("<span>BS Computer Science</span>"); }
    if ($(this).is(':contains("bsee")'))
    { $(".jqury-titel").append("<span>BS Electrical Engineering</span>"); }
  
    if ($(this).is(':contains("ahss")'))
    { $(".jqury-titel").append("<span>School of Arts, Humanities & Social Sciences</span>"); }
    if ($(this).is(':contains("cnd")'))
    { $(".jqury-titel").append("<span>BA (Honors) Communication & Design</span>"); }
    if ($(this).is(':contains("ch")'))
    { $(".jqury-titel").append("<span>BA (Honors) Comparative Humanities</span>"); }
    if ($(this).is(':contains("sdp")'))
    { $(".jqury-titel").append("<span>BSc (Honors) Social Development & Policy</span>"); }
  });
  
  // $('.jqury-titel span').each(function(){ 
  //   var text = $(this).html();
  //   text = text.slice(0, -2);
  //   $(this).html(text);
  // });

  
    if ($(".jqury-url").is(':contains("ahss")'))
    {
  var ahsskeyword = pageURL.split("ahss")[1];
  var ahsstemp = ahsskeyword.split(",");
  if(ahsstemp.length<=1)
    {
      $(".jqury-titel").hide();
      $(".archive-title").show();
    }
  else {
      $(".archive-description").hide(); 
    }
    } else {
  var keyword = pageURL.split("dsse")[1];
  var temp = keyword.split(",");
  if(temp.length<=1)
    {
      $(".jqury-titel").hide();
      $(".archive-title").show();
    }
  else {
    $(".archive-description").hide();
    }
    }
  });