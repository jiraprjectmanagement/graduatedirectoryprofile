<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Habib_Graduate_Directory
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">


	<?php wp_enqueue_style( 'style-single-cpt-03', get_template_directory_uri() . '/css/style-single-cpt-03.css',false,'1.1','all'); ?>
  <?php wp_enqueue_style( 'responsive-03.css', get_template_directory_uri() . '/css/responsive-03.css',false,'1.1','all'); ?>

  <?php wp_enqueue_script( 'preloader js script', get_template_directory_uri() . '/external-plugin/preloader/preloader.js',false,'1.1','all'); ?>
	<?php wp_enqueue_style( 'preloader', get_template_directory_uri() . '/external-plugin/preloader/preloader.css',false,'1.1','all'); ?>

  <?php wp_enqueue_style( 'social-share', get_template_directory_uri() . '/external-plugin/social-share/social-share.css',false,'1.1','all'); ?>

  <?php wp_enqueue_script( 'slimscroll js script', get_template_directory_uri() . '/external-plugin/slimscroll/slimscroll.js',false,'1.1','all'); ?>
  
  <?php wp_enqueue_script( 'custom-03 js script', get_template_directory_uri() . '/js/custom-03.js',false,'1.1','all'); ?>
  
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" />
</head>

<body <?php body_class(); ?> id="aspiration-profiles">

<!-- Start: Loading spinner --> 
<div id="page-overlay" class="loading">
    <span aria-hidden="true" aria-label="Loading"></span>
    <!-- <?php //echo __('Loading ... ')?> -->
        <?php 
        $title_for_animation = get_the_title();
        $title_for_animation = str_split($title_for_animation);
        echo '<h1 class="loading-title">';
         foreach ($title_for_animation as $key => $value) {
                echo "<span>" . $value . "</span>";
            }
            echo "</h1>";
        // echo '<h1 class="entry-title">' . $title_for_animation . '</h1>';
        // the_title( '<h1 class="entry-title">', '</h1>' );
        ?>
    <div class="loading-bar">
      <div class="progress-bar"></div>
    </div>
</div> 
<!-- End: Loading spinner --> 


<?php wp_body_open(); ?>
<div id="page" class="site">
<!-- Custom Popup -->
<div class="custom-popup">
  <div class="custom-pop-inner">
      <a href="javascript:;" class="cross-close">x</a>
      <div class="item-box">
          <h3>Office of Career Services</h3>
          <p>Habib University, Block 18, Gulistan-e-Jauhar, University Avenue، Off Shahrah-e-Faisal Rd, Karachi - 75290, Sindh, Pakistan</p>
      </div>
      <div class="item-box">
        <div class="contact-area"> 
          <p><a href="tel:+9221111042242"><i class="fa fa-phone"></i>+92 21 1110 42242 (HABIB)</a></p>
          <p><a href="mailto:career.services@habib.edu.pk"><i class="fa fa-envelope" aria-hidden="true"></i>career.services@habib.edu.pk</a></p>
        </div>
      </div>
      <div class="item-box">
        <div class="popup_social">
            <h3>Follow Habib University</h3>
            <a href="https://www.facebook.com/HabibUniversity" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
            <a href="https://www.linkedin.com/company/habib-university" title="LinkedIn" target="_blank"><i class="fab fa-linkedin-in"></i></a>
            <a href="https://instagram.com/habibuniversity" title="Instagram" target="_blank"><i class="fab fa-instagram"></i></a> 
            <a href="https://twitter.com/habibuniversity" title="Twitter" target="_blank"><i class="fab fa-twitter"></i></a>       
            <a href="https://www.youtube.com/user/HabibUni" title="YouTube" target="_blank"><i class="fab fa-youtube"></i></a>
            <a href="hhttp://habib.edu.pk/" title="website" target="_blank"><i class="fas fa-globe"></i></a>
        </div>
      </div>
    </div>
</div>
<!-- Custom Popup -->

	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'graduate-directory' ); ?></a>


<header class="header-profile">
  <div class="container">
    <div class="header-inner">
      <div class="logo">
          <img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/logo.svg" alt="">
      </div>
      <div class="header-right">
           <?php
              wp_nav_menu(
                array(
                  'theme_location' => 'menu-1',
                  'menu_id' => 'primary-menu',
                  'menu_class' => 'prof-nav',
                  )
                );
            ?>
            <a href="javascript:;" class="back-to-list-res">Back to list</a>
            <a href="javascript:;" class="contact-us-btn btn-default gardi-dflt menu-toggle">Menu</a>

    
      </div>
    </div>
  </div>
</header>




<!-- <header id="masthead" class="site-header header-profile">
	<div class="container px-lg-5 site-branding">
        <div class="navbar navbar-expand-lg navbar-light  main-nav header-inner">
            <div class="logo">
                <?php the_custom_logo(); ?>
            </div>
            
          <nav id="site-navigation" class="main-navigation nav-center">
            <button class="menu-toggle navbar-toggler" type="button" data-toggle="collapse" aria-controls="primary-menu" aria-expanded="false">
              <div id="nav-icon3"><span></span><span></span><span></span><span></span></div>
            </button>
            <?php
              wp_nav_menu(
                array(
                  'theme_location' => 'menu-1',
                  'menu_id' => 'primary-menu',
                  )
                );
            ?>
              <div class="main-btn">
                <a href="#footer" class="btn" style=""><span class="btn-txt-one">Contact Us</span><span class="btn-txt-two">Contact Us</span></a>
            </div>
          </nav>
        </div>
      </div>
	</header> -->
