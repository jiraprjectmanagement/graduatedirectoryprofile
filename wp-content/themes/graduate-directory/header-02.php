<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Habib_Graduate_Directory
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

  <link href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/lity/dist/lity.css" rel="stylesheet">
  <script src="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/lity/vendor/jquery.js"></script>
  <script src="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/lity/dist/lity.js"></script>

  <link rel="stylesheet" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/owlcarousel/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/owlcarousel/owl.theme.default.min.css">
  <!-- <script src="<?php //echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/owlcarousel/jquery.min.js"></script> -->
  <script src="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/owlcarousel/owl.carousel.min.js"></script>


	<?php wp_enqueue_style( 'style-single-cpt-02', get_template_directory_uri() . '/css/style-single-cpt-02.css',false,'1.1','all'); ?>
  <?php wp_enqueue_style( 'responsive.css', get_template_directory_uri() . '/css/responsive.css',false,'1.1','all'); ?>
  <?php wp_enqueue_style( 'responsive.css', get_template_directory_uri() . '/css/responsive-2.css',false,'1.1','all'); ?>


  <?php wp_enqueue_script( 'Custom js script', get_template_directory_uri() . '/js/custom-02.js',false,'1.1','all'); ?>
  
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css" />
</head>

<body <?php body_class(); ?> id="aspiration-profiles">
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'graduate-directory' ); ?></a>


<header class="header-profile">
  <div class="container">
    <div class="header-inner">
      <div class="logo">
          <?php the_custom_logo(); ?>
      </div>
      <div class="header-right">
          <ul class="prof-nav">
            <li><a href="">Home</a></li>
            <li><a href="">Convocation</a></li>
            <li><a href="">Alumni</a></li>
            <li><a href="" class="contact-us-btn btn-default">Contact Us</a></li>
          </ul>
      </div>
    </div>
  </div>
</header>




<!-- <header id="masthead" class="site-header header-profile">
	<div class="container px-lg-5 site-branding">
        <div class="navbar navbar-expand-lg navbar-light  main-nav header-inner">
            <div class="logo">
                <?php the_custom_logo(); ?>
            </div>
            
          <nav id="site-navigation" class="main-navigation nav-center">
            <button class="menu-toggle navbar-toggler" type="button" data-toggle="collapse" aria-controls="primary-menu" aria-expanded="false">
              <div id="nav-icon3"><span></span><span></span><span></span><span></span></div>
            </button>
            <?php
              wp_nav_menu(
                array(
                  'theme_location' => 'menu-1',
                  'menu_id' => 'primary-menu',
                  )
                );
            ?>
              <div class="main-btn">
                <a href="#footer" class="btn" style=""><span class="btn-txt-one">Contact Us</span><span class="btn-txt-two">Contact Us</span></a>
            </div>
          </nav>
        </div>
      </div>
	</header> -->
