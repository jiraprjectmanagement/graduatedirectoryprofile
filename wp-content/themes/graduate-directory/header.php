<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Habib_Graduate_Directory
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

  <link href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/lity/dist/lity.css" rel="stylesheet">
  <script src="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/lity/vendor/jquery.js"></script>
  <script src="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/lity/dist/lity.js"></script>
	
	<link rel="stylesheet" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/owlcarousel/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/owlcarousel/owl.theme.default.min.css">
  <!-- <script src="<?php //echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/owlcarousel/jquery.min.js"></script> -->
  <script src="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/owlcarousel/owl.carousel.min.js"></script>

  <?php wp_enqueue_script( 'preloader js script', get_template_directory_uri() . '/external-plugin/preloader/preloader.js',false,'1.1','all'); ?>
	<!-- <?php //wp_enqueue_style( 'preloader', get_template_directory_uri() . '/external-plugin/preloader/preloader.css',false,'1.1','all'); ?> -->
  <link rel="stylesheet" rel="preload" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/external-plugin/preloader/preloader.css" as="style">


  <?php //wp_enqueue_style( 'social-share', get_template_directory_uri() . '/external-plugin/social-share/social-share.css',false,'1.1','all'); ?>

  <link rel="stylesheet" rel="preload" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/css/style-header-footer.css" as="style">
  <link rel="stylesheet" rel="preload" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/css/style-habib.css" as="style">
  <link rel="stylesheet" rel="preload" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/css/style-cpt.css" as="style">
  <!-- <link rel="stylesheet" rel="preload" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/css/style-single-cpt.css" as="style"> -->
  <link rel="stylesheet" rel="preload" href="<?php echo get_site_url(null, '/wp-content/themes/graduate-directory'); ?>/css/responsive.css" as="style">

  <?php wp_enqueue_script( 'Custom js script', get_template_directory_uri() . '/js/Custom.js',false,'1.1','all'); ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-PQ18G345G6"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'G-PQ18G345G6');
</script>
    <!-- Global site tag (gtag.js) - Google Analytics -->

</head>
<body <?php body_class(); ?>>

<!-- Start: Loading spinner --> 
<div id="page-overlay" class="loading">
    <span aria-hidden="true" aria-label="Loading">
        <!-- <?php //echo __('Loading ... ')?> -->
    </span>
</div> 
<!-- End: Loading spinner --> 

<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'graduate-directory' ); ?></a>

	<header id="masthead" class="site-header main-header">
	<div class="container-fluid px-lg-5 site-branding">
        <div class="navbar navbar-expand-lg navbar-light  main-nav">
            <div class="logo">
                <?php the_custom_logo(); ?>

            </div>
            
            <nav id="site-navigation" class="main-navigation nav-center">
			    <button class="menu-toggle navbar-toggler" type="button" data-toggle="collapse" aria-controls="primary-menu" aria-expanded="false">
				    <?php //esc_html_e( 'Primary Menu', 'graduate-directory' ); ?>
					<div id="nav-icon3">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
			    </button>

                <?php
			    wp_nav_menu(
				    array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				    )
			    );
			    ?>

              <!-- <div class="main-btn">
                <a href="#footer" class="btn" style=""><span class="btn-txt-one">Contact Us</span><span class="btn-txt-two">Contact Us</span></a>
              </div> -->
                </nav>
                <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" href="/wellness-center/">Home</a>
                    </li>
                    ================================= <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">News</a>
                          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li> 
                                <a class="dropdown-item" href="#">USA</a>
                                <ul class="sub-menu">
                                    <a class="dropdown-item" target="_blank" href="https://habib.edu.pk/HU-news/houston-community-committed-to-supporting-habib-university/">Houston Community Committed to Supporting Habib University</a>
                                </ul>
                            </li>
                            <li> 
                                <a class="dropdown-item" href="javascript:;">Pakistan</a>
                                  <ul class="sub-menu">
                                    <a class="dropdown-item" target="_blank" href="https://www.facebook.com/mohsineenofhu/posts/659582344797155">Mr. Salman Mirajwala visits Habib  <br> University Campus</a>                                       
                                  </ul>
                            </li>
                          </ul>
                    </li> ============================================
                    <li class="nav-item dropdown">
                      <a class="nav-link" href="/wellness-center/#ourservices">Our Services</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/wellness-center/#Schedule">Schedule</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/wellness-center/#contact">Contact Us</a>
                    </li>
                    <li class="nav-item site-link">
                      <a class="nav-link main-sit-link-img" href="http://habib.edu.pk/"><img src="assets/img/hu-favicon.ico" class="sit-link-img" alt="" style="width: 30px;">Visit HU Website</a>
                    </li>
                  </ul>
                </div>
            </nav>
            =================================== <div class="mobile-dropdown desck-giv">
                <div class="dropdown">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    Give Now
                    </button>
                    <div class="dropdown-menu">
                    <a class="dropdown-item" href="https://giving.hufus.org/">Donate <img src="assets/img/arrow-drop.svg" alt=""></a>
                    <a class="dropdown-item" href="https://hufus.org/support">Pledge <img src="assets/img/arrow-drop.svg" alt=""></a>
                    </div>
                </div>
            </div> =========================-->

            <!---<div class="search">
              <div class="home link">
                <a href="http://habib.edu.pk/" class="main-sit-link-img">
                  <img src="assets/img/hu-favicon.ico" class="sit-link-img" alt="" style="width: 30px;">
                  Visit HU Website
                </a>
                ========================= <a href="http://habib.edu.pk/" class="main-sit-link-i">
                  <i class="fa fa-home"></i>
                </a> =========================
              </div>
              ========================= <form role="search" action="http://google.com/search">
                <input type="text" name="q" class="query live-search-query" value="" placeholder="Type something..." autocomplete="off">
                <input name="q" type="hidden" value="https://sfortech.com/"/>
                  <button name="submit" class="search-button" type="submit" value="Search"><i class="fa fa-search"></i></button>
                  <div class="live-search-results"></div>
              </form> =========================-->
        	</div>
    	</div>
	</header><!-- #masthead -->
