<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Habib_Graduate_Directory
 */


/*
  Template Name: single-graduate-01
  Template Post Type: post, page, graduate
 */

get_header('01'); ?>
	<main id="primary" class="single site-main">
<!-- 
		<select id="changetemplate" name="changetemplate">
            <option value="0">Template 01</option>
            <option value="1">Template 02</option>
		</select> -->


		<div class="back-page-button">
			<!-- Share Btn -->
			<div class="main-share">
				<?php  echo do_shortcode('[SSB]'); ?>
			</div>
			<!-- Share Btn -->
			<div class="right-share-btn">
				<div class="main-share-btn">
					<a href="javascript:void(0)" class="share-btn"> <img src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/04/share-icon.svg" alt=""> Share Profile</a>
				</div>
				<div class="main-btn-BACK">
					<a href="javascript:void(0);" class="back-btn btn-default gardi-dflt" onclick="goBack()">
					  <div class="over-area">
						 <span data-hover="Back to list">  Back to list </span>
					  </div>
				   </a>
				</div>
			</div>
		</div>

		<?php while ( have_posts() ) : the_post(); ?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



	<div class="single-main-post-content">
		<div class="single-post-left">
			<div id="profile" class="profile-section single-profile">
			<div class="single-information">
					<div class="single-main-post-thumbnail">
						<?php 	if ( has_post_thumbnail() ) {
								the_post_thumbnail();
							} else { ?>
								<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/05/no-user-picture.png" alt="<?php the_title(); ?>" />
							<?php } ?>
						<?php //graduate_directory_post_thumbnail(); ?>	
					</div>
					<div class="single-information-content">
						<?php if( get_field('video_profile_link') ): ?>	
							<div class="video-link">
								<a class="gardi-dflt" href="<?php the_field('video_profile_link'); ?>" data-lity>
									<!-- <i class="fa fa-play" aria-hidden="true"></i>  -->
									<img class="wtch-video" src="https://habib.edu.pk/graduate-directory/wp-content/uploads/2022/05/play-icon-2.svg" alt="">
									<div class="over-area">
										<span data-hover="Watch Video Profile"> Watch Video Profile  </span>
									</div>
								</a>
							</div>
						<?php endif; ?>
					<header class="entry-header">
						<?php
							if ( is_singular() ) :
								the_title( '<h1 class="entry-title">', '</h1>' );
							else :
								the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
							endif;

							if ( 'post' === get_post_type() ) :
						?>
						<div class="entry-meta">
							<?php
								graduate_directory_posted_on();
								graduate_directory_posted_by();
							?>
						</div>
							<?php endif; ?>
					</header>
					<div class="class-name">
						<?php
							$categories = get_the_terms( $post->ID, 'class' );
							if (is_array($categories) || is_object($categories)) {
								foreach( $categories as $category ) {
									// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
									// echo $category->name . '</a><br />';
									echo $category->name . '<br />';
								}
							}
						?>
					</div>
					<div class="program-name">
						<?php
							$categories = get_the_terms( $post->ID, 'program' );
							if (is_array($categories) || is_object($categories)) {
							foreach( $categories as $category ) {
									//echo $category->term_id . ', ' . $category->slug . ', ' . $category->name . '<br />';
								if(!$category->parent == 0)  {
									// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
									// echo $category->name .'</a><br />'; }
									echo $category->name .'<br />'; }
							}
						 } ?>
					</div>
					<!-- <div class="minors"> -->
						<?php
							$categories = get_the_terms( $post->ID, 'minor' );
							if (is_array($categories) || is_object($categories)) {
								foreach( $categories as $category ) {
									// echo '<a class="" href="'. get_category_link( $category->term_id ) .'" >';
									// echo $category->name . '</a><br />';
									echo '<div class="minors"><span>Minor: </span>' . $category->name . '<br /></div>';
								}
							}
						?>
					<!-- </div> -->
					
					<div class="single-contect-info">
					
							<div class="habib_email">
								<?php if( get_field('habib_email') ): ?>
									<i class="fa fa-envelope" aria-hidden="true"></i><a class="" href="mailto:<?php the_field('habib_email'); ?>"> <?php the_field('habib_email'); ?></a>
								<?php endif; ?>
							</div>
							<div class="email">
								<?php if( get_field('email') ): ?>
									<i class="fa fa-envelope" aria-hidden="true"></i><a class="" href="mailto:<?php the_field('email'); ?>"> <?php the_field('email'); ?></a>
								<?php endif; ?>
							</div>
							<!-- <div class="phone">
								<?php if( get_field('phone') ): ?>
									<i class="fa fa-phone" aria-hidden="true"></i><a class="" href="tel:<?php the_field('phone'); ?>"> <?php the_field('phone'); ?></a>
								<?php endif; ?>
							</div> -->
							<div class="linkedin_link">
								<?php if( get_field('linkedin_link') ): ?>
									<i class="fab fa-linkedin-in"></i><a class="" href="<?php the_field('linkedin_link'); ?>" target="_blank"> Linkedin Profile</a>
								<?php endif; ?>
							</div>
							<div class="portfolio_link">
								<?php if( get_field('portfolio_link') ): ?>
									<i class="fa fa-globe" aria-hidden="true"></i><a class="" href="<?php the_field('portfolio_link'); ?>" target="_blank"><?php the_field('portfolio_Title'); ?></a>
								<?php endif; ?>
							</div>
					</div>
				</div>
			</div>

			
		</div>
	</div>





		<div class="single-post-right">
			<!-- <div class="entry-content">
				 <?php  // the_content( sprintf( wp_kses( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'graduate-directory' ),
						//	array(
						//		'span' => array(
						//		'class' => array(),
						//	), ) ),
						//wp_kses_post( get_the_title() ) ) );
						//wp_link_pages( array(
						//	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'graduate-directory' ),
						//	'after'  => '</div>', ) ); 	?>
			</div> -->
			
			<?php if( get_field('aspiration_statement') ): ?>
				<div class="aspiration_statement">
					<h1 class="main-heading">Aspiration Statement</h1>
					<p class=""><?php the_field('aspiration_statement'); ?></p>
				</div>
				<?php endif; ?>
				<?php if( get_field('core_skills') ): ?>
					<div id="core_skills" class="profile-section single-core-skills">
					<div class="core_skills">
					<!-- <h3>Core Skills</h3> -->
					<h2>Core Skills</h2>
					<?php 
						$rows = get_field('core_skills');
							if( $rows ) {
    							echo '<ul class="skill widthmeasure">';
    								foreach( $rows as $row ) {
        							// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
									echo '<li>';
									echo $row['skill'];
       								echo '</li>';
    							}
   						 	echo '</ul>';
						}
					?>
			</div>
			</div>
				<?php endif; ?>
				
				<?php if( get_field('academic_awards') ): ?>
					<div id="academic" class="profile-section single-academic">
				<div class="Academic Awards">
					<h2>Academic Awards / Achievements</h2>
				<?php 
						$rows = get_field('academic_awards');
							if( $rows ) {
    							echo '<ul class="widthmeasure">';
    								foreach( $rows as $row ) {
        							// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
									echo '<li>';
									echo $row['academic_record'];
       								echo '</li>';
    							}
   						 	echo '</ul>';
						}
					?>
			</div>
		</div>
				<?php endif; ?>
			

		<div id="experience" class="profile-section single-experience">
			<div class="leadership-experiences">
			<h2 class="main-heading">Experience</h2>
				<?php if( get_field('leadership-experiences') ): ?>
					<h3 class="color-heading">Leadership /Meta-curricular</h3>
				<?php endif; ?>
					<?php 
						$rows = get_field('leadership-experiences');
							if( $rows ) {
    							echo '<ul class="leadership widthmeasure">';
    								foreach( $rows as $row ) {
        							// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
									echo '<li>';
									echo $row['leadership'];
       								echo '</li>';
    							}
   						 	echo '</ul>';
						}
					?>
			</div>
			<div class="work-experience">
				<?php if( get_field('work-experience') ): ?>
					<h3 class="color-heading">Internship / Volunteer Work</h3>
				<?php endif; ?>
					<?php 
						$rows = get_field('work-experience');
							if( $rows ) {
    							echo '<ul class="experience widthmeasure">';
    								foreach( $rows as $row ) {
        							// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
									echo '<li>';
									echo $row['experience'];
       								echo '</li>';
    							}
   						 	echo '</ul>';
						}
					?>
			</div>
		</div>
		<?php if( get_field('publications-projects') ): ?>	
		<div id="publications" class="profile-section single-publications">
				<div class="publications-projects">
							<?php 
							
							$rows = get_field('publications-projects');
							if( $rows ) {
								echo '<h2 class="main-heading">Publications or Creative Projects</h2>';
									echo '<ul class="publications widthmeasure">';
										foreach( $rows as $row ) {
										// echo '<li><i class="fa fa-check" aria-hidden="true"></i>';
										echo '<li>';
										echo $row['publications'];
										   echo '</li>';
									}
									echo '</ul>';
							}
							?>
							</div>
	
				</div>
				<?php endif; ?>
				
				<?php if( get_field('fyp_desc') ||  get_field('project_pictures') || get_field('project-video') ): ?>
					<div id="fyp" class="profile-section single-fyp">
					<div class="fyp_desc">
					<h2 class="main-heading">Final Year Project</h2>
				<?php endif; ?>



				<?php if( get_field('fyp_desc') ): ?>
						<h3 class="color-heading">Description</h3>
						<p class="project_title"><?php the_field('project_title'); ?></p>
						<p><?php the_field('fyp_desc'); ?></p>
				<?php endif; ?>
			</div>
			<div class="project_pictures">
				<?php if( get_field('project_pictures') ): ?>
					<h3 class="color-heading">Project Pictures</h3>
				<?php endif; ?>
			<?php 
					$rows = get_field('project_pictures');
						if( $rows ) {
    					echo '<div class="owl-carousel">';
    						foreach( $rows as $row ) {
        						$image = $row['project_pic'];
        						echo '<div>';
							?> 
								<a href="<?php echo $image['url']; ?>" data-lity>
									<!-- <img src="<?php echo $image['sizes']['full-size']; ?>" alt="<?php echo $image['alt']; ?>" /> -->
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	   							</a>
	   							<!-- <p><?php //echo $image['caption']; ?></p> -->
	   						<?php
        						echo '</div>';
    						}
   						 echo '</div>';
						}?>
			</div>
			<div class="project-video">

				<?php if( get_field('project-video') ): ?>
					<h3 class="color-heading">Project Video</h3>
				<?php endif; ?>
				<?php if( get_field('project-video') ): ?>
						<!-- <p><?php the_field('project-video'); ?></p> -->
						<a class="" href="<?php the_field('project-video'); ?>" target="_blank"><i class="fa fa-link" aria-hidden="true"></i> <span>_______</span><span> Watch Video</span></a>
					<?php endif; ?>
			</div>
		</div>
	</div>

		
	</div>

	<footer class="entry-footer">
		<?php //graduate_directory_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	<div class="previ-next-warpper">
			<?php
				the_post_navigation(
					array(
						'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous', 'graduate-directory' ) . '</span>',
						'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next', 'graduate-directory' ) . '</span>',
					)
				);
			?>
		</div>
</article><!-- #post-<?php the_ID(); ?> -->
<?php
			//get_template_part( 'template-parts/content', get_post_type() );

			// the_post_navigation(
			// 	array(
			// 		'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'graduate-directory' ) . '</span> <span class="nav-title">%title</span>',
			// 		'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'graduate-directory' ) . '</span> <span class="nav-title">%title</span>',
			// 	)
			// );
			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;



		
		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
//get_sidebar();
get_footer(01);
