<?php
/**
 * Habib Graduate Directory functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Habib_Graduate_Directory
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function graduate_directory_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on Habib Graduate Directory, use a find and replace
		* to change 'graduate-directory' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'graduate-directory', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'graduate-directory' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'graduate_directory_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'graduate_directory_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function graduate_directory_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'graduate_directory_content_width', 640 );
}
add_action( 'after_setup_theme', 'graduate_directory_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function graduate_directory_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'graduate-directory' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'graduate-directory' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'graduate_directory_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function graduate_directory_scripts() {
	wp_enqueue_style( 'graduate-directory-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'graduate-directory-style', 'rtl', 'replace' );

	wp_enqueue_script( 'graduate-directory-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'graduate_directory_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//////////////////////////////////////////////////////////////////////////
////////////////////////  Start My Function //////////////////////////////
//////////////////////////////////////////////////////////////////////////

/*----- SVG upload in media ------*/
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml'; 
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types'); 


/*======= CPT rigister =========*/

/*------- All Custom Post Type UI Post Types ------------ */

function cptui_register_my_cpts() {

	/**
	 * Post Type: graduates.
	 */

	$labels = [
		"name" => __( "graduates", "graduate-directory" ),
		"singular_name" => __( "graduate", "graduate-directory" ),
	];

	$args = [
		"label" => __( "graduates", "graduate-directory" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => false,
		"rewrite" => [ "slug" => "graduate", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-welcome-learn-more",
		"supports" => [ "title", "editor", "thumbnail", "excerpt", "trackbacks", "custom-fields", "comments", "revisions", "author", "page-attributes", "post-formats" ],
		"taxonomies" => [ "program", "school", "class" ],
		"show_in_graphql" => false,
	];

	register_post_type( "graduate", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );

/*------- All Custom Post Type UI Taxonomies ------------ */

function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Programs.
	 */

	$labels = [
		"name" => __( "Programs", "graduate-directory" ),
		"singular_name" => __( "Program", "graduate-directory" ),
		"menu_name" => __( "Programs", "graduate-directory" ),
		"all_items" => __( "All Programs", "graduate-directory" ),
	];

	
	$args = [
		"label" => __( "Programs", "graduate-directory" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'program', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "program",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "program", [ "graduate" ], $args );

	/**
	 * Taxonomy: Schools.
	 */

	$labels = [
		"name" => __( "Schools", "graduate-directory" ),
		"singular_name" => __( "School", "graduate-directory" ),
	];

	
	$args = [
		"label" => __( "Schools", "graduate-directory" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'school', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "school",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "school", [ "graduate" ], $args );

	/**
	 * Taxonomy: Classes.
	 */

	$labels = [
		"name" => __( "Classes", "graduate-directory" ),
		"singular_name" => __( "Class", "graduate-directory" ),
	];

	
	$args = [
		"label" => __( "Classes", "graduate-directory" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'class', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "class",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "class", [ "graduate" ], $args );

	/**
	 * Taxonomy: Minors.
	 */

	$labels = [
		"name" => __( "Minors", "graduate-directory" ),
		"singular_name" => __( "Minor", "graduate-directory" ),
	];

	
	$args = [
		"label" => __( "Minors", "graduate-directory" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'minor', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "minor",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"sort" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "minor", [ "graduate" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes' );


/*------- Programs Taxonomy ------------ */

/*------- Schools Taxonomy ------------ */

/*------- Classes Taxonomy ------------ */

/*------- Minors  Taxonomy ------------ */



/*----- Social Share Template include ------*/
// function wcr_share_buttons() {
//     $share_url      = urlencode(get_the_permalink());
//     $share_title    = urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));
//     $media          = urlencode(get_the_post_thumbnail_url(get_the_ID(), 'full'));

//     include( locate_template('external-plugin/social-share/social-share.php', false, false) );
// }


/*----- Set Default Template ------*/
function default_post_template() {
    global $post;
    if ( 'graduate' == $post->post_type 
        && 0 != count( get_page_templates( $post ) ) 
        && get_option( 'page_for_posts' ) != $post->ID // Not the page for listing posts
        && '' == $post->page_template // Only when page_template is not set
    ) {
        $post->page_template = "single-graduate-01.php";
    }
}
add_action('add_meta_boxes', 'default_post_template', 1);


/*----- Hide Default Content Editor form graduate Post ------*/
add_action( 'init', function() {
	remove_post_type_support( 'graduate', 'editor' );
	}, 99);


/*----- Role Class in DashBoard body tag ------*/
function role_admin_body_class( $classes ) {
	global $current_user;
	foreach( $current_user->roles as $role )
		$classes .= ' role-' . $role;
	return trim( $classes );
}
add_filter( 'admin_body_class', 'role_admin_body_class' );


/*----- Enqueue script in Dashboard ------*/
add_action( 'admin_print_scripts-post-new.php', 'graduate_admin_script', 11 );
add_action( 'admin_print_scripts-post.php', 'graduate_admin_script', 11 );
add_action( 'admin_enqueue_scripts', 'graduate_admin_script', 11 );
function graduate_admin_script() {
		// global $post_type;
		// if( 'graduate' == $post_type )
		wp_enqueue_script( 'admin-script', get_stylesheet_directory_uri() . '/js/admin.js' );
}
add_action( 'admin_enqueue_scripts', 'graduate_admin_style', 11 );
function graduate_admin_style() {
		// global $post_type;
		// if( 'graduate' == $post_type )
		wp_register_style( 'admin-style', get_stylesheet_directory_uri() . '/css/admin.css', false, '1.0.0' );
		wp_enqueue_style( 'admin-style' );
		//wp_enqueue_style( 'admin-style', get_stylesheet_directory_uri() . '/css/admin.css' );
}

/*----- Automatically Set Parent Taxonomy When Select Child Taxonomy ------*/
	add_action( 'set_object_terms', 'auto_set_parent_terms', 9999, 6 );
/*
 * Automatically set/assign parent taxonomy terms to posts
 * This function will automatically set parent taxonomy terms whenever terms are set on a post,
 * with the option to configure specific post types, and/or taxonomies.
 * @param int    $object_id  Object ID.
 * @param array  $terms      An array of object terms.
 * @param array  $tt_ids     An array of term taxonomy IDs.
 * @param string $taxonomy   Taxonomy slug.
 * @param bool   $append     Whether to append new terms to the old terms.
 * @param array  $old_tt_ids Old array of term taxonomy IDs.
 */
function auto_set_parent_terms( $object_id, $terms, $tt_ids, $taxonomy, $append, $old_tt_ids ) {
    /**
     * We only want to move forward if there are taxonomies to set
     */
    if( empty( $tt_ids ) ) return FALSE;
    /**
     * Set specific post types to only set parents on.  Set $post_types = FALSE to set parents for ALL post types.
     */
    $post_types = array( 'graduate' );
    if( $post_types !== FALSE && ! in_array( get_post_type( $object_id ), $post_types ) ) return FALSE;
    /**
     * Set specific taxonomy types to only set parents on.  Set $tax_types = FALSE to set parents for ALL taxonomies of this post types.
     */
    $tax_types = array( 'Programs' );
    if( $tax_types !== FALSE && ! in_array( get_post_type( $object_id ), $post_types ) ) return FALSE;
    foreach( $tt_ids as $tt_id ) {
        $parent = wp_get_term_taxonomy_parent_id( $tt_id, $taxonomy );
        if( $parent ) {
            wp_set_post_terms( $object_id, array($parent), $taxonomy, TRUE );
        }
    }
}