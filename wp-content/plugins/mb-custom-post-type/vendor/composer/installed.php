<?php return array(
    'root' => array(
        'pretty_version' => '2.3.1',
        'version' => '2.3.1.0',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '542ab2f18924849acf91f1ddb4e85b9da4011866',
        'name' => 'wpmetabox/mb-custom-post-type',
        'dev' => true,
    ),
    'versions' => array(
        'meta-box/support' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../meta-box/support',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => '3a02b1a2391afeb31751241d38606b2b0368da7b',
            'dev_requirement' => false,
        ),
        'wpmetabox/mb-custom-post-type' => array(
            'pretty_version' => '2.3.1',
            'version' => '2.3.1.0',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '542ab2f18924849acf91f1ddb4e85b9da4011866',
            'dev_requirement' => false,
        ),
    ),
);
